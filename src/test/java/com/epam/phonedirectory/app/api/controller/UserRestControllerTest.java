package com.epam.phonedirectory.app.api.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.annotation.DirtiesContext.MethodMode;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@SpringJUnitWebConfig(locations = {
    "file:src/main/webapp/WEB-INF/persistence.xml",
    "file:src/main/webapp/WEB-INF/spring-security.xml",
    "file:src/main/webapp/WEB-INF/app-servlet.xml",
    "file:src/main/webapp/WEB-INF/view-resolver.xml",
    "file:src/main/webapp/WEB-INF/exception-handler.xml"
})
@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD, classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
class UserRestControllerTest {

  private MockMvc mockMvc;

  @Autowired
  private WebApplicationContext webApplicationContext;

  @NotNull
  private static SecurityMockMvcRequestPostProcessors.UserRequestPostProcessor getUser() {
    return user("vaddemgen@gmail.com")
        .password("q1w2e3r4")
        .roles("REGISTERED_USER", "BOOKING_MANAGER");
  }

  @BeforeEach
  public void setUp() {
    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
        .apply(springSecurity())
        .build();
  }

  @Test
  void getUserJson() throws Exception {
    mockMvc.perform(
        get("/api/v1/users/1")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .accept(MediaType.APPLICATION_JSON)
            .with(getUser()))
        .andExpect(status().isOk())
        .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
        .andExpect(jsonPath("$.id").isNumber())
        .andExpect(jsonPath("$.firstName").isString())
        .andExpect(jsonPath("$.lastName").isString())
        .andExpect(jsonPath("$.email").isString())
        .andExpect(jsonPath("$.phones").isArray());
  }

  @Test
  void getUserPdf() throws Exception {
    mockMvc.perform(
        get("/api/v1/users/1")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .accept(MediaType.APPLICATION_PDF)
            .with(getUser()))
        .andExpect(status().isOk())
        .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE));
  }

  @Test
  void createUserJson() throws Exception {
    mockMvc.perform(
        post("/api/v1/users")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .with(csrf().asHeader())
            .with(getUser())
            .content(
                "{\n"
                    + "\t\"firstName\": \"John\",\n"
                    + "\t\"lastName\": \"Smith\",\n"
                    + "\t\"email\": \"test@gmail.com\",\n"
                    + "\t\"password\": \"asdfasdf\",\n"
                    + "\t\"confirmPassword\": \"asdfasdf\",\n"
                    + "\t\"roles\": [\"USER\", \"BOOKING_MANAGER\"],\n"
                    + "\t\"phoneTypes\": [\"home\"],\n"
                    + "\t\"phoneNumbers\": [\"+380501111112\"],\n"
                    + "\t\"phoneCompanies\": [\"Test\"]\n"
                    + "}"))
        .andExpect(status().isCreated())
        .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
        .andExpect(jsonPath("$.id").isNumber())
        .andExpect(jsonPath("$.firstName").value("John"))
        .andExpect(jsonPath("$.lastName").value("Smith"))
        .andExpect(jsonPath("$.email").value("test@gmail.com"))
        .andExpect(jsonPath("$.phones").isArray());
  }

  @Test
  void createUserPdf() throws Exception {
    mockMvc.perform(
        post("/api/v1/users")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_PDF)
            .with(csrf().asHeader())
            .with(getUser())
            .content(
                "{\n"
                    + "\t\"firstName\": \"John\",\n"
                    + "\t\"lastName\": \"Smith\",\n"
                    + "\t\"email\": \"test2@gmail.com\",\n"
                    + "\t\"password\": \"asdfasdf\",\n"
                    + "\t\"confirmPassword\": \"asdfasdf\",\n"
                    + "\t\"roles\": [\"USER\", \"BOOKING_MANAGER\"],\n"
                    + "\t\"phoneTypes\": [\"home\"],\n"
                    + "\t\"phoneNumbers\": [\"+380501111112\"],\n"
                    + "\t\"phoneCompanies\": [\"Test\"]\n"
                    + "}"))
        .andExpect(status().isCreated())
        .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE));
  }

  @Test
  void updateUserJson() throws Exception {
    mockMvc.perform(
        put("/api/v1/users/1")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .with(csrf().asHeader())
            .with(getUser())
            .content(
                "{\n"
                    + "\t\"firstName\": \"John\",\n"
                    + "\t\"lastName\": \"Smith\",\n"
                    + "\t\"email\": \"test3@gmail.com\"\n"
                    + "}"))
        .andExpect(status().isOk())
        .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
        .andExpect(jsonPath("$.id").isNumber())
        .andExpect(jsonPath("$.firstName").value("John"))
        .andExpect(jsonPath("$.lastName").value("Smith"))
        .andExpect(jsonPath("$.email").value("test3@gmail.com"))
        .andExpect(jsonPath("$.phones").isArray());
  }

  @Test
  void updateUserPdf() throws Exception {
    mockMvc.perform(
        put("/api/v1/users/1")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_PDF)
            .with(csrf().asHeader())
            .with(getUser())
            .content(
                "{\n"
                    + "\t\"firstName\": \"John\",\n"
                    + "\t\"lastName\": \"Smith\",\n"
                    + "\t\"email\": \"test4@gmail.com\"\n"
                    + "}"))
        .andExpect(status().isOk())
        .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE));
  }

  @Test
  void getUsersJson() throws Exception {
    mockMvc.perform(
        get("/api/v1/users")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .accept(MediaType.APPLICATION_JSON)
            .with(getUser())
            .param("page", "1")
            .param("size", "2")
            .param("sort", "email,asc"))
        .andExpect(status().isOk())
        .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
        .andExpect(jsonPath("$.number").value(1))
        .andExpect(jsonPath("$.size").isNumber())
        .andExpect(jsonPath("$.content").isArray());
  }

  @Test
  void getUsersPdf() throws Exception {
    mockMvc.perform(
        get("/api/v1/users")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .accept(MediaType.APPLICATION_PDF)
            .with(getUser())
            .param("page", "1")
            .param("size", "2")
            .param("sort", "email,asc"))
        .andExpect(status().isOk())
        .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE));
  }
}