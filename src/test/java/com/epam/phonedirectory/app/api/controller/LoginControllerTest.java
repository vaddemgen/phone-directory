package com.epam.phonedirectory.app.api.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringJUnitWebConfig(locations = {
    "file:src/main/webapp/WEB-INF/persistence.xml",
    "file:src/main/webapp/WEB-INF/spring-security.xml",
    "file:src/main/webapp/WEB-INF/app-servlet.xml",
    "file:src/main/webapp/WEB-INF/view-resolver.xml",
    "file:src/main/webapp/WEB-INF/exception-handler.xml"
})
class LoginControllerTest {

  private MockMvc mockMvc;

  @Autowired
  private WebApplicationContext webApplicationContext;

  @BeforeEach
  public void setUp() {
    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
        .apply(springSecurity())
        .build();
  }

  @Test
  void login() throws Exception {
    mockMvc.perform(
        post("/login")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .accept(MediaType.APPLICATION_JSON)
            .with(csrf().asHeader())
            .param("username", "vaddemgen@gmail.com")
            .param("password", "q1w2e3r4")
    ).andExpect(status().isFound());
  }
}