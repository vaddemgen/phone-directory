package com.epam.phonedirectory.app.persistencelayer.repository;

import com.epam.phonedirectory.app.persistencelayer.model.UserAccountEntity;
import java.util.Optional;
import java.util.Set;
import javax.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserAccountRepository extends JpaRepository<UserAccountEntity, Long> {

  Optional<UserAccountEntity> findOneByNumberId(long phoneNumberId);

  @Query("select ua from UserAccount ua left join fetch ua.number n left join fetch n.phoneCompany left join fetch ua.operator left join fetch ua.user u where u.id = :userId")
  Set<UserAccountEntity> getAllByUserId(@Param("userId") long userId);

  @Lock(LockModeType.PESSIMISTIC_WRITE)
  Optional<UserAccountEntity> findOneByIdAndUserId(long userId, long accountId);
}
