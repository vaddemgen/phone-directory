package com.epam.phonedirectory.app.persistencelayer;

import com.epam.phonedirectory.api.model.User;
import com.epam.phonedirectory.app.persistencelayer.mapper.EntityMapper;
import com.epam.phonedirectory.app.persistencelayer.model.UserEntity;
import com.epam.phonedirectory.app.persistencelayer.repository.UserRepository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public final class UserDataMapperImpl implements UserDataMapper {

  private final EntityMapper entityMapper;
  private final UserRepository userRepository;

  @Override
  public Optional<User> getByUserId(long userId) {
    return userRepository.findOneByIdDeep(userId)
        .map(entityMapper::toUserDeep);
  }

  @Override
  public Optional<User> getByEmail(String email) {
    return userRepository.findOneByEmail(email)
        .map(entityMapper::toUserDeep);
  }

  @Override
  public void removeUserById(long userId) {
    userRepository.deleteById(userId);
  }

  @Override
  public User updateUser(User user) {
    // Looking for user entity by id.
    UserEntity userEntity = userRepository.getOne(user.getId());

    // Applying changes.
    userEntity.setFirstName(user.getFirstName());
    userEntity.setLastName(user.getLastName());
    userEntity.setEmail(user.getEmail());

    // Saving and converting back to the domain model.
    return entityMapper.toUserDeep(userRepository.save(userEntity));
  }

  @Override
  public User addUser(User user) {
    // Converting to the DB entity.
    UserEntity userEntity = entityMapper.toUserEntity(user);
    // Saving and converting back to the domain model.
    return entityMapper.toUserDeep(userRepository.save(userEntity));
  }

  @Override
  public Page<User> getUsers(Pageable pageable) {
    return userRepository.findAllDeep(pageable)
        .map(entityMapper::toUserDeep);
  }

  @Override
  public List<User> getUsers() {
    return userRepository.findAllDeep()
        .stream()
        .map(entityMapper::toUserDeep)
        .collect(Collectors.toList());
  }
}
