package com.epam.phonedirectory.app.persistencelayer;

import com.epam.phonedirectory.api.model.User;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserDataMapper {

  List<User> getUsers();

  Optional<User> getByUserId(long userId);

  Optional<User> getByEmail(String email);

  void removeUserById(long userId);

  User updateUser(User user);

  User addUser(User user);

  Page<User> getUsers(Pageable pageable);
}
