package com.epam.phonedirectory.app.persistencelayer.repository;

import com.epam.phonedirectory.app.persistencelayer.model.PhoneCompanyEntity;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneCompanyRepository extends JpaRepository<PhoneCompanyEntity, Long> {

  Optional<PhoneCompanyEntity> findOneByName(String name);
}
