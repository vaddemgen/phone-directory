package com.epam.phonedirectory.app.persistencelayer;

import com.epam.phonedirectory.api.model.UserAccount;
import com.epam.phonedirectory.app.persistencelayer.mapper.EntityMapper;
import com.epam.phonedirectory.app.persistencelayer.model.UserAccountEntity;
import com.epam.phonedirectory.app.persistencelayer.repository.PhoneCompanyRepository;
import com.epam.phonedirectory.app.persistencelayer.repository.PhoneNumberRepository;
import com.epam.phonedirectory.app.persistencelayer.repository.UserAccountRepository;
import com.epam.phonedirectory.app.persistencelayer.repository.UserRepository;
import java.util.Optional;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@Slf4j
public final class UserAccountDataMapperImpl implements UserAccountDataMapper {

  private final EntityMapper entityMapper;
  private final UserAccountRepository userAccountRepository;
  private final UserRepository userRepository;
  private final PhoneNumberRepository phoneNumberRepository;
  private final PhoneCompanyRepository phoneCompanyRepository;

  @Override
  public void addUserAccount(UserAccount userAccount) {
    UserAccountEntity entity = userAccountRepository.save(
        UserAccountEntity.builder()
            .amount(userAccount.getAmount())
            .user(userRepository.getOne(userAccount.getUser().getId()))
            .number(phoneNumberRepository.getOne(userAccount.getPhoneNumber().getId()))
            .operator(phoneCompanyRepository.getOne(userAccount.getOperator().getId()))
            .build()
    );
    log.trace("USER_ACCOUNT_CREATE: Created entity {}", entity);
  }

  @Override
  public Stream<UserAccount> getAllByUserId(long userId) {
    return userAccountRepository.getAllByUserId(userId)
        .stream()
        .map(entityMapper::toUserAccount);
  }

  @Override
  public void removeUserAccount(UserAccount model) {
    userAccountRepository.deleteById(model.getId());
    userAccountRepository.flush();
  }

  @Override
  public Optional<UserAccount> getByUserIdAndAccountId(long userId, long accountId) {
    return userAccountRepository.findOneByIdAndUserId(accountId, userId)
        .map(entityMapper::toUserAccount);
  }

  @Override
  public void updateUserAccount(UserAccount userAccount) {
    UserAccountEntity entity = userAccountRepository.getOne(userAccount.getId());
    entity.setAmount(userAccount.getAmount());
    entity.setOperator(phoneCompanyRepository.getOne(userAccount.getOperator().getId()));
    userAccountRepository.saveAndFlush(entity);
  }

  @Override
  public Optional<UserAccount> getByPhoneNumber(String number) {
    var phoneNumberEntity = phoneNumberRepository.findOneByNumber(number).orElseThrow();
    return userAccountRepository.findOneByNumberId(phoneNumberEntity.getId())
        .map(entityMapper::toUserAccount);
  }
}
