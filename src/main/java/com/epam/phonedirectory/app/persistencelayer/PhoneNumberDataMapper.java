package com.epam.phonedirectory.app.persistencelayer;

import com.epam.phonedirectory.api.model.PhoneNumber;
import com.epam.phonedirectory.api.model.User;
import java.util.Optional;

public interface PhoneNumberDataMapper {

  Optional<PhoneNumber> getByNumber(String phoneNumber);

  PhoneNumber updatePhoneNumber(PhoneNumber model);

  void assignPhoneNumber(PhoneNumber model, User user);

  PhoneNumber addPhoneNumber(PhoneNumber model, User user);
}
