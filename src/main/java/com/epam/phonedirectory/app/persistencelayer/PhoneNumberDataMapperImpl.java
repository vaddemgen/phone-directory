package com.epam.phonedirectory.app.persistencelayer;

import com.epam.phonedirectory.api.model.PhoneNumber;
import com.epam.phonedirectory.api.model.User;
import com.epam.phonedirectory.app.persistencelayer.mapper.EntityMapper;
import com.epam.phonedirectory.app.persistencelayer.model.PhoneNumberEntity;
import com.epam.phonedirectory.app.persistencelayer.repository.PhoneCompanyRepository;
import com.epam.phonedirectory.app.persistencelayer.repository.PhoneNumberRepository;
import com.epam.phonedirectory.app.persistencelayer.repository.UserRepository;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public final class PhoneNumberDataMapperImpl implements PhoneNumberDataMapper {

  private final EntityMapper entityMapper;
  private final PhoneNumberRepository repository;
  private final UserRepository userRepository;
  private final PhoneCompanyRepository phoneCompanyRepository;

  @Override
  public Optional<PhoneNumber> getByNumber(String phoneNumber) {
    return repository.findOneByNumber(phoneNumber)
        .map(entityMapper::toPhoneNumber);
  }

  @Override
  public PhoneNumber updatePhoneNumber(PhoneNumber number) {
    // Looking for an existed entity.
    var entity = repository.findById(number.getId()).orElseThrow();

    // Merging data
    entity.setNumber(number.getNumber());
    entity.setType(number.getType());
    entity.setPhoneCompany(phoneCompanyRepository.getOne(number.getPhoneCompany().getId()));

    // Saving and converting back to the domain model.
    return entityMapper.toPhoneNumber(repository.save(entity));
  }

  @Override
  public void assignPhoneNumber(PhoneNumber phoneNumber, User user) {
    PhoneNumberEntity entity = repository.getOne(phoneNumber.getId());
    entity.setUser(userRepository.getOne(user.getId()));
    repository.save(entity);
  }

  @Override
  public PhoneNumber addPhoneNumber(PhoneNumber model, User user) {
    // Converting domain model to the DB entity.
    var entity = entityMapper.toPhoneNumberEntity(model);

    // Updating the entity dependencies.
    entity.setPhoneCompany(phoneCompanyRepository.getOne(model.getPhoneCompany().getId()));
    entity.setUser(userRepository.getOne(user.getId()));

    // Saving model and converting back to the domain model.
    return entityMapper.toPhoneNumber(repository.save(entity));
  }
}
