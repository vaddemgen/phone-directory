package com.epam.phonedirectory.app.persistencelayer;

import com.epam.phonedirectory.api.model.UserAuthDetail;
import java.util.Optional;

public interface UserAuthDetailDataMapper {

  UserAuthDetail addUserAuthDetail(UserAuthDetail model);

  Optional<UserAuthDetail> getByUserId(long id);

  void updateUserAuthDetail(UserAuthDetail model);

  Optional<UserAuthDetail> getByEmail(String email);
}
