package com.epam.phonedirectory.app.persistencelayer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(name = "PhoneNumber")
@Table(name = "phone_numbers")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PhoneNumberEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(updatable = false, insertable = false)
  private long id;

  @Column(nullable = false, length = 50)
  @NotBlank
  @Size(max = 50)
  private String type;

  @Column(nullable = false, length = 50)
  @NotBlank
  @Size(max = 50)
  @Pattern(regexp = "^\\+(?:[0-9] ?){6,14}[0-9]$")
  private String number;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "user_id")
  @NotNull
  private UserEntity user;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "phone_company_id", nullable = false)
  @NotNull
  private PhoneCompanyEntity phoneCompany;
}
