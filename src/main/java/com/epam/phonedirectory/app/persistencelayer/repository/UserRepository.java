package com.epam.phonedirectory.app.persistencelayer.repository;

import com.epam.phonedirectory.app.persistencelayer.model.UserEntity;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

  @Query("select distinct u from User u left join fetch u.phones p left join fetch p.phoneCompany")
  List<UserEntity> findAllDeep();

  Optional<UserEntity> findOneByEmail(String email);

  @Query("select u from User u left join fetch u.phones p left join fetch p.phoneCompany where u.id = :id")
  Optional<UserEntity> findOneByIdDeep(@Param("id") long userId);

  @Query(value = "select distinct u from User u left join fetch u.phones p left join fetch p.phoneCompany",
      countQuery = "select count(u) from User u")
  Page<UserEntity> findAllDeep(Pageable pageable);
}
