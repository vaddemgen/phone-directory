package com.epam.phonedirectory.app.persistencelayer.repository;

import com.epam.phonedirectory.app.persistencelayer.model.PhoneNumberEntity;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneNumberRepository extends JpaRepository<PhoneNumberEntity, Long> {

  Optional<PhoneNumberEntity> findOneByNumber(String number);
}
