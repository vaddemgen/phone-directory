package com.epam.phonedirectory.app.persistencelayer;

import com.epam.phonedirectory.api.model.UserAccount;
import java.util.Optional;
import java.util.stream.Stream;

public interface UserAccountDataMapper {

  /**
   * Returns created {@link UserAccount}.
   */
  void addUserAccount(UserAccount userAccount);

  Stream<UserAccount> getAllByUserId(long userId);

  Optional<UserAccount> getByPhoneNumber(String number);

  void removeUserAccount(UserAccount model);

  Optional<UserAccount> getByUserIdAndAccountId(long userId, long accountId);

  void updateUserAccount(UserAccount userAccount);
}
