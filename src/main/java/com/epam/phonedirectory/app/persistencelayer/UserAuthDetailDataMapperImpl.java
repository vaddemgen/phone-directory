package com.epam.phonedirectory.app.persistencelayer;

import com.epam.phonedirectory.api.model.UserAuthDetail;
import com.epam.phonedirectory.app.persistencelayer.mapper.EntityMapper;
import com.epam.phonedirectory.app.persistencelayer.model.UserAuthDetailEntity;
import com.epam.phonedirectory.app.persistencelayer.repository.UserAuthDetailRepository;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public final class UserAuthDetailDataMapperImpl implements UserAuthDetailDataMapper {

  private final EntityMapper entityMapper;
  private final UserAuthDetailRepository repository;

  @Override
  public UserAuthDetail addUserAuthDetail(UserAuthDetail userAuthDetail) {
    // Converting the domain model to the DB entity.
    UserAuthDetailEntity entity = entityMapper.toUserAuthDetailEntity(userAuthDetail);
    // Updating user id with the actual value.
    entity.setUserId(userAuthDetail.getUser().getId());
    // Saving the entity and converting back to the domain model.
    return entityMapper.toUserAuthDetail(repository.save(entity));
  }

  @Override
  public Optional<UserAuthDetail> getByUserId(long id) {
    return repository.findById(id)
        .map(entityMapper::toUserAuthDetail);
  }

  @Override
  public void updateUserAuthDetail(UserAuthDetail model) {
    // Looking for the DB entity in the DB.
    UserAuthDetailEntity entity = repository.getOne(model.getUser().getId());

    // Applying changes.
    entity.setPassword(model.getPassword());
    entity.setRoles(entityMapper.rolesToString(model.getRoles()));
    entity.setUsername(model.getUsername());

    // Saving and converting to the domain model.
    repository.save(entity);
  }

  @Override
  public Optional<UserAuthDetail> getByEmail(String email) {
    return repository.findOneByUsername(email)
        .map(entityMapper::toUserAuthDetail);
  }
}
