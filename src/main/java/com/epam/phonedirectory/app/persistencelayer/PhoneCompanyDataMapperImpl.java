package com.epam.phonedirectory.app.persistencelayer;

import com.epam.phonedirectory.api.model.PhoneCompany;
import com.epam.phonedirectory.app.persistencelayer.mapper.EntityMapper;
import com.epam.phonedirectory.app.persistencelayer.repository.PhoneCompanyRepository;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public final class PhoneCompanyDataMapperImpl implements PhoneCompanyDataMapper {

  private final EntityMapper entityMapper;
  private final PhoneCompanyRepository repository;

  @Override
  public Optional<PhoneCompany> getByCompanyName(String companyName) {
    return repository.findOneByName(companyName)
        .map(entityMapper::toPhoneCompany);
  }

  @Override
  public Optional<PhoneCompany> getByCompanyId(long companyId) {
    return repository.findById(companyId)
        .map(entityMapper::toPhoneCompany);
  }

  @Override
  public PhoneCompany addPhoneCompany(PhoneCompany model) {
    // Creating a DB entity.
    var entity = entityMapper.toPhoneCompanyEntity(model);
    // Saving and converting back to a domain model.
    return entityMapper.toPhoneCompany(repository.save(entity));
  }

  @Override
  public Set<PhoneCompany> getAll() {
    return repository.findAll()
        .stream()
        .map(entityMapper::toPhoneCompany)
        .collect(Collectors.toSet());
  }
}
