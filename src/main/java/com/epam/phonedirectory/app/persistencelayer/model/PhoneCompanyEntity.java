package com.epam.phonedirectory.app.persistencelayer.model;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(name = "PhoneCompany")
@Table(name = "phone_companies")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PhoneCompanyEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(updatable = false, insertable = false)
  private long id;

  @Column(nullable = false, length = 50, unique = true)
  @NotBlank
  @Size(max = 50)
  private String name;

  @OneToMany(mappedBy = "phoneCompany", cascade = CascadeType.ALL)
  private Set<PhoneNumberEntity> phones;
}
