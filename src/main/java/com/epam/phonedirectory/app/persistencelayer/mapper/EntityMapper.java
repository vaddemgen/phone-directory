package com.epam.phonedirectory.app.persistencelayer.mapper;

import com.epam.phonedirectory.api.model.PhoneCompany;
import com.epam.phonedirectory.api.model.PhoneNumber;
import com.epam.phonedirectory.api.model.Role;
import com.epam.phonedirectory.api.model.User;
import com.epam.phonedirectory.api.model.UserAccount;
import com.epam.phonedirectory.api.model.UserAuthDetail;
import com.epam.phonedirectory.app.businesslayer.model.UserAuthDetailImpl;
import com.epam.phonedirectory.app.persistencelayer.model.PhoneCompanyEntity;
import com.epam.phonedirectory.app.persistencelayer.model.PhoneNumberEntity;
import com.epam.phonedirectory.app.persistencelayer.model.UserAccountEntity;
import com.epam.phonedirectory.app.persistencelayer.model.UserAuthDetailEntity;
import com.epam.phonedirectory.app.persistencelayer.model.UserEntity;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface EntityMapper {

  @Mapping(target = "phones", ignore = true)
  @Mapping(target = "userAuthDetail", ignore = true)
  @Mapping(target = "id", ignore = true)
  UserEntity toUserEntity(User model);

  User toUserDeep(UserEntity entity);

  @Mapping(target = "userId", source = "user.id")
  @Mapping(target = "user", ignore = true)
  @Mapping(target = "roles", expression = "java(rolesToString(model.getRoles()))")
  UserAuthDetailEntity toUserAuthDetailEntity(UserAuthDetail model);

  @Mapping(target = "roles", expression = "java(rolesToSet(entity.getRoles()))")
  @Mapping(target = "user.phones", ignore = true)
  UserAuthDetailImpl toUserAuthDetail(UserAuthDetailEntity entity);

  default String rolesToString(Stream<Role> roles) {
    return roles.map(Role::getValue)
        .collect(Collectors.joining(","));
  }

  default Set<Role> rolesToSet(String roles) {
    return Stream.of(roles.split(","))
        .filter(StringUtils::isNotBlank)
        .map(Role::fromString)
        .collect(Collectors.toSet());
  }

  @Mapping(source = "companyName", target = "name")
  @Mapping(target = "phones", ignore = true)
  PhoneCompanyEntity toPhoneCompanyEntity(PhoneCompany model);

  @Mapping(source = "name", target = "companyName")
  PhoneCompany toPhoneCompany(PhoneCompanyEntity entity);

  @Mapping(target = "user", ignore = true)
  @Mapping(target = "phoneCompany", ignore = true)
  PhoneNumberEntity toPhoneNumberEntity(PhoneNumber phoneNumber);

  PhoneNumber toPhoneNumber(PhoneNumberEntity phoneNumber);

  @Mapping(source = "number", target = "phoneNumber")
  @Mapping(target = "user.phones", ignore = true)
  UserAccount toUserAccount(UserAccountEntity entity);
}
