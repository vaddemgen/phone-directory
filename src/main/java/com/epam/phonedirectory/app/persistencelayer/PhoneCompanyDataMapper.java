package com.epam.phonedirectory.app.persistencelayer;

import com.epam.phonedirectory.api.model.PhoneCompany;
import java.util.Optional;
import java.util.Set;

public interface PhoneCompanyDataMapper {

  Optional<PhoneCompany> getByCompanyName(String companyName);

  Optional<PhoneCompany> getByCompanyId(long companyId);

  PhoneCompany addPhoneCompany(PhoneCompany model);

  Set<PhoneCompany> getAll();
}
