package com.epam.phonedirectory.app.persistencelayer.model;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(name = "User")
@Table(name = "users")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(updatable = false, insertable = false)
  private long id;

  @Column(name = "first_name", nullable = false, length = 50)
  @NotBlank
  @Size(max = 50)
  private String firstName;

  @Column(name = "last_name", nullable = false, length = 50)
  @NotBlank
  @Size(max = 50)
  private String lastName;

  @Column(nullable = false, length = 254, unique = true)
  @NotBlank
  @Size(max = 254)
  private String email;

  @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
  private Set<PhoneNumberEntity> phones;

  @OneToOne(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  private UserAuthDetailEntity userAuthDetail;
}
