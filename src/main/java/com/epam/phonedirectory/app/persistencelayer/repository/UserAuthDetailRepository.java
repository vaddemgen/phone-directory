package com.epam.phonedirectory.app.persistencelayer.repository;

import com.epam.phonedirectory.app.persistencelayer.model.UserAuthDetailEntity;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAuthDetailRepository extends JpaRepository<UserAuthDetailEntity, Long> {

  Optional<UserAuthDetailEntity> findOneByUsername(String username);
}
