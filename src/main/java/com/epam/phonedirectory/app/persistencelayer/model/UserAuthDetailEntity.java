package com.epam.phonedirectory.app.persistencelayer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Table(name = "users_auth_details")
@Entity(name = "UserAuthDetail")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@EqualsAndHashCode(doNotUseGetters = true, exclude = "user")
public class UserAuthDetailEntity {

  @Id
  @Column(name = "user_id", updatable = false)
  private long userId;

  @OneToOne
  @JoinColumn(name = "user_id", insertable = false, updatable = false)
  private UserEntity user;

  @Column(nullable = false, length = 254, unique = true)
  @NotBlank
  @Size(max = 254)
  private String username;

  @Column
  @Nullable
  @Size(max = 255)
  private String roles;

  @Column(nullable = false)
  @NotBlank
  @Size(max = 255)
  private String password;

  @Column(name = "account_non_expired", nullable = false)
  private boolean accountNonExpired;

  @Column(name = "account_non_locked", nullable = false)
  private boolean accountNonLocked;

  @Column(name = "credentials_non_expired", nullable = false)
  private boolean credentialsNonExpired;

  @Column(nullable = false)
  private boolean enabled;
}
