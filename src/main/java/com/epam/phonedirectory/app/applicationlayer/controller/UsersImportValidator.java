package com.epam.phonedirectory.app.applicationlayer.controller;

import static java.util.stream.Collectors.toSet;

import com.epam.phonedirectory.api.model.User;
import com.epam.phonedirectory.app.businesslayer.exception.ValidationException;
import java.text.MessageFormat;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.Validation;
import javax.validation.Validator;
import lombok.extern.slf4j.Slf4j;

@Slf4j
final class UsersImportValidator {

  private UsersImportValidator() {
  }

  static void validateUsers(Set<User> users) throws ValidationException {
    Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    var violations = users.stream()
        .flatMap(user -> validator.validate(user).stream())
        .collect(toSet());

    if (!violations.isEmpty()) {
      log.debug("IMPORT_USERS: violations: {}", violations);
      String message = violations.stream()
          .map(violation -> MessageFormat
              .format("''{0}'' {1}", violation.getPropertyPath(), violation.getMessage()))
          .collect(Collectors.joining(",\n"));
      throw new ValidationException(message);
    }
  }
}
