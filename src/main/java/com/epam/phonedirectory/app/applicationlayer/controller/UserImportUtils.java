package com.epam.phonedirectory.app.applicationlayer.controller;

import com.epam.phonedirectory.api.model.User;
import com.epam.phonedirectory.app.businesslayer.exception.InvalidUsersImportFile;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;

@Slf4j
final class UserImportUtils {

  private UserImportUtils() {
  }

  static Set<User> readUsers(InputStream io) throws InvalidUsersImportFile {
    try {
      return Set.of(
          new Gson().fromJson(
              new BufferedReader(new InputStreamReader(io, StandardCharsets.UTF_8)),
              User[].class)
      );
    } catch (JsonParseException e) {
      log.debug("IMPORT_USERS: The source file is not valid", e);
      throw new InvalidUsersImportFile(e);
    }
  }
}
