package com.epam.phonedirectory.app.applicationlayer.model;

import java.util.Comparator;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;

@SuppressWarnings("WeakerAccess") // Must be public to access it in a view.
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@EqualsAndHashCode(doNotUseGetters = true)
@ToString(doNotUseGetters = true)
public final class PhoneCompanyPresenter implements Comparable<PhoneCompanyPresenter> {

  private final long id;
  private final String companyName;

  @Override
  public int compareTo(@NotNull PhoneCompanyPresenter o) {
    return Comparator.comparing(PhoneCompanyPresenter::getCompanyName)
        .compare(this, o);
  }
}
