package com.epam.phonedirectory.app.applicationlayer.model;

import java.util.Comparator;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Getter
public class UserAccountPresenter implements Comparable<UserAccountPresenter> {

  private final long id;
  private final String accountNumber;
  private final String amount;
  private final PhoneNumberPresenter phoneNumber;
  private final PhoneCompanyPresenter operator;

  @Override
  public int compareTo(@NotNull UserAccountPresenter o) {
    return Comparator.comparing(UserAccountPresenter::getAccountNumber)
        .compare(this, o);
  }
}
