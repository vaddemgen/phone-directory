package com.epam.phonedirectory.app.applicationlayer.model;

import static java.util.Collections.emptyList;
import static java.util.Objects.nonNull;

import com.epam.phonedirectory.api.model.Role;
import java.util.List;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.jetbrains.annotations.Nullable;

@AllArgsConstructor
@Getter
@ToString(doNotUseGetters = true, exclude = {"password", "confirmPassword"})
public class UserForm {

  @NotBlank
  @Pattern(regexp = "[a-zA-Z]+")
  @Size(max = 50)
  private final String firstName;

  @NotBlank
  @Pattern(regexp = "[a-zA-Z]+")
  @Size(max = 50)
  private final String lastName;

  @NotBlank
  @Email
  @Size(max = 254)
  private final String email;

  @Pattern(regexp = "^$|\\S{4,50}")
  private final String password;

  @Pattern(regexp = "^$|\\S{4,50}")
  private final String confirmPassword;

  @Nullable
  private final List<Role> roles;

  @Nullable
  private final List<String> phoneTypes;

  @Nullable
  private final List<String> phoneNumbers;

  @Nullable
  private final List<String> phoneCompanies;

  public List<String> getPhoneTypes() {
    return nonNull(phoneTypes) ? phoneTypes : emptyList();
  }

  public List<String> getPhoneNumbers() {
    return nonNull(phoneNumbers) ? phoneNumbers : emptyList();
  }

  public List<String> getPhoneCompanies() {
    return nonNull(phoneCompanies) ? phoneCompanies : emptyList();
  }

  public List<Role> getRoles() {
    return nonNull(roles) ? roles : emptyList();
  }
}
