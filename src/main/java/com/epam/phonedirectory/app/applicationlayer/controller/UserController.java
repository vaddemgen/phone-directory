package com.epam.phonedirectory.app.applicationlayer.controller;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import com.epam.phonedirectory.api.exception.ModelNotFoundException;
import com.epam.phonedirectory.api.model.Role;
import com.epam.phonedirectory.api.model.User;
import com.epam.phonedirectory.api.service.UserAuthDetailService;
import com.epam.phonedirectory.api.service.UserService;
import com.epam.phonedirectory.app.applicationlayer.model.UserForm;
import com.epam.phonedirectory.app.applicationlayer.model.UserMapper;
import com.epam.phonedirectory.app.applicationlayer.model.UsersListPresenter;
import com.epam.phonedirectory.app.businesslayer.report.UsersReport;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.stream.Collectors;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("users")
@Slf4j
@AllArgsConstructor
public class UserController {

  private final UserMapper userMapper;
  private final UserService userService;
  private final UserAuthDetailService userAuthDetailService;

  @GetMapping
  public String index(ModelMap model) {
    model.addAttribute("data", new UsersListPresenter(
        userService.getUsers()
            .map(userMapper::userToUserPresenter)
            .collect(toSet())));
    return "users/list";
  }

  @GetMapping(value = "pdf", headers = "Accept=application/pdf")
  public String generateReport(Model model) {
    model.addAttribute("report",
        UsersReport.builder()
            .name("Users")
            .date(LocalDateTime.now())
            .users(
                userService.getUsers()
                    .map(userMapper::userToUserPresenter)
                    .collect(toSet())
            )
            .build()
    );
    return "usersReportView";
  }

  @GetMapping("new")
  public String showAddNewUserPage() {
    return "users/new";
  }

  @PostMapping("new")
  public String createUser(@Valid UserForm form, ModelMap model) {

    log.debug("CREATE_USER: {}", form);

    User user = userMapper.createUserFormToModel(form);

    if (!StringUtils.equals(form.getPassword(), form.getConfirmPassword())) {
      model.addAttribute("validationError", "Passwords don't match.");
      model.addAttribute("user", userMapper.userToUserPresenter(user));
      model.addAttribute("roles",
          form.getRoles().stream().map(Role::toString).collect(toList()));
      return "users/new";
    }

    userService.createUser(user, form.getPassword(), form.getRoles());

    return "redirect:/users";
  }

  @GetMapping("{userId}")
  public String showEditUserPage(@PathVariable long userId,
      ModelMap model) throws ModelNotFoundException {
    User user = userService.getUser(userId);
    model.addAttribute("user",
        userMapper.userToUserPresenter(user));
    model.addAttribute("roles",
        userAuthDetailService.getUserAuthDetail(user.getEmail())
            .getRoles()
            .map(Role::toString)
            .collect(toList()));
    return "users/edit";
  }

  @PostMapping("{userId}/edit")
  public String updateUser(@PathVariable long userId, @Valid UserForm form,
      ModelMap model) {

    log.debug("EDIT_USER: User ID: '{}', form data: '{}'", userId, form);

    User user = userMapper.createUserFormToModel(form)
        .withId(userId);

    if (!StringUtils.equals(form.getPassword(), form.getConfirmPassword())) {
      model.addAttribute("validationError", "Passwords don't match.");
      model.addAttribute("user",
          userMapper.userToUserPresenter(user));
      model.addAttribute("roles",
          form.getRoles().stream().map(Role::toString).collect(toList()));
      return "users/edit";
    }

    userService.updateUser(user,
        StringUtils.isBlank(form.getPassword()) ? null : form.getPassword(),
        form.getRoles());

    return "redirect:/users";
  }

  @PostMapping("{userId}/delete")
  public String deleteUser(@PathVariable long userId) {
    log.trace("DELETE_USER: User ID: {}", userId);
    userService.deleteUser(userId);
    return "redirect:/users";
  }

  @ExceptionHandler(BindException.class)
  public ModelAndView handleValidationException(BindException e) {
    log.trace("CREATE_USER: Validation exception", e);
    String errors = e.getFieldErrors().stream()
        .map(fieldError ->
            MessageFormat.format(
                "The value ''{0}'' is not valid for the field ''{1}'': {2}",
                fieldError.getRejectedValue(),
                fieldError.getField(),
                fieldError.getDefaultMessage()
            )
        ).collect(Collectors.joining("; "));
    return new ModelAndView("users/new")
        .addObject("model", new ModelMap("validationError",
            MessageFormat.format("Failed due to validation errors: {0}", errors)));
  }
}
