package com.epam.phonedirectory.app.applicationlayer.controller;

import com.epam.phonedirectory.api.exception.NotEnoughMoneyException;
import com.epam.phonedirectory.api.exception.UserAccountNotFoundException;
import com.epam.phonedirectory.api.model.User;
import com.epam.phonedirectory.api.service.UserAccountService;
import com.epam.phonedirectory.api.service.UserAuthDetailService;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Random;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("userAccounts")
@AllArgsConstructor
@Slf4j
public class UserAccountController {

  private final UserAuthDetailService userAuthDetailService;
  private final UserAccountService userAccountService;

  @PostMapping("{accountId}/topUp")
  public String topUp(@PathVariable long accountId, Principal principal)
      throws UserAccountNotFoundException {
    log.trace("USER_ACCOUNT_TOP_UP: Account '{}', User '{}'", accountId, principal.getName());
    double randomValue = new Random().nextDouble() * 10D;

    User user = userAuthDetailService.getUserAuthDetail(principal.getName()).getUser();
    userAccountService.topUpAccount(user, accountId, randomValue);

    return "redirect:/profile?success="
        + URLEncoder.encode(MessageFormat.format(
        "You have successfully replenished your account with {0}",
        NumberFormat.getCurrencyInstance().format(randomValue)
    ), StandardCharsets.UTF_8);
  }

  @PostMapping("{accountId}/changeOperator")
  public String changeOperator(@PathVariable long accountId, @RequestParam long operatorId,
      Principal principal) throws NotEnoughMoneyException, UserAccountNotFoundException {

    log.trace("USER_ACCOUNT_CHANGE_OPERATOR: Account '{}', New Operator '{}', User '{}'", accountId,
        operatorId, principal.getName());

    User user = userAuthDetailService.getUserAuthDetail(principal.getName()).getUser();
    userAccountService.changeOperator(user, accountId, operatorId);

    return "redirect:/profile?success="
        + URLEncoder.encode("You have successfully changed your operator", StandardCharsets.UTF_8);
  }

  @ExceptionHandler({NotEnoughMoneyException.class, UserAccountNotFoundException.class})
  public String handleException(Exception e) {
    return "redirect:/profile?error="
        + URLEncoder.encode(e.getMessage(), StandardCharsets.UTF_8);
  }
}
