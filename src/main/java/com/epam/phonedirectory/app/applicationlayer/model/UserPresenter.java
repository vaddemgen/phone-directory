package com.epam.phonedirectory.app.applicationlayer.model;

import static java.util.Objects.nonNull;

import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode(doNotUseGetters = true)
@ToString(doNotUseGetters = true)
public final class UserPresenter {

  private final long id;
  private final String firstName;
  private final String lastName;
  private final String email;
  private final Set<PhoneNumberPresenter> phones;

  @Builder
  private UserPresenter(long id, String firstName, String lastName, String email,
      Set<PhoneNumberPresenter> phones) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    TreeSet<PhoneNumberPresenter> treeSet = new TreeSet<>(
        Comparator.comparing(PhoneNumberPresenter::getType)
            .thenComparing(PhoneNumberPresenter::getNumber)
            .thenComparing(PhoneNumberPresenter::getPhoneCompany,
                Comparator.comparing(PhoneCompanyPresenter::getCompanyName))
    );
    if (nonNull(phones)) {
      treeSet.addAll(phones);
    }
    this.phones = Collections.unmodifiableSet(treeSet);
  }
}
