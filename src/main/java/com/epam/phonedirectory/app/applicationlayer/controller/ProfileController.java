package com.epam.phonedirectory.app.applicationlayer.controller;

import com.epam.phonedirectory.api.exception.ModelNotFoundException;
import com.epam.phonedirectory.api.model.UserAuthDetail;
import com.epam.phonedirectory.api.service.PhoneCompanyService;
import com.epam.phonedirectory.api.service.UserAccountService;
import com.epam.phonedirectory.api.service.UserService;
import com.epam.phonedirectory.app.applicationlayer.model.PhoneCompanyMapper;
import com.epam.phonedirectory.app.applicationlayer.model.UserAccountMapper;
import com.epam.phonedirectory.app.applicationlayer.model.UserMapper;
import java.util.TreeSet;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("profile")
@Controller
@AllArgsConstructor
public class ProfileController {

  private final UserService userService;
  private final UserAccountService userAccountService;
  private final UserMapper userMapper;
  private final PhoneCompanyService phoneCompanyService;
  private final UserAccountMapper userAccountMapper;
  private final PhoneCompanyMapper phoneCompanyMapper;

  @GetMapping
  public String showProfile(Authentication authentication,
      ModelMap model,
      @RequestParam(name = "success", required = false) String successMessage,
      @RequestParam(name = "error", required = false) String errorMessage
  ) throws ModelNotFoundException {
    UserAuthDetail authDetail = (UserAuthDetail) authentication.getPrincipal();
    model.addAttribute("user", userMapper.userToUserPresenter(
        userService.getUser(authDetail.getUser().getId())));
    model.addAttribute("accounts",
        userAccountService.getAccounts(authDetail.getUser())
            .map(userAccountMapper::toUserAccountPresenter)
            .collect(Collectors.toCollection(TreeSet::new))
    );
    model.addAttribute("operators",
        phoneCompanyService.getAll()
            .map(phoneCompanyMapper::toPhoneCompanyPresenter)
            .collect(Collectors.toCollection(TreeSet::new))
    );
    if (StringUtils.isNoneBlank(successMessage)) {
      model.addAttribute("successMessage", successMessage);
    }
    if (StringUtils.isNoneBlank(errorMessage)) {
      model.addAttribute("errorMessage", errorMessage);
    }
    return "profile";
  }
}
