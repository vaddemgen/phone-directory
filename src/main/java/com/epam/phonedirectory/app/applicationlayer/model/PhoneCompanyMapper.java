package com.epam.phonedirectory.app.applicationlayer.model;

import com.epam.phonedirectory.api.model.PhoneCompany;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PhoneCompanyMapper {

  PhoneCompanyPresenter toPhoneCompanyPresenter(PhoneCompany model);
}
