package com.epam.phonedirectory.app.applicationlayer.model;

import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import lombok.Getter;

@Getter
public final class UsersListPresenter {

  private final int size;
  private final Set<UserPresenter> users;

  public UsersListPresenter(Set<UserPresenter> users) {
    TreeSet<UserPresenter> treeSet = new TreeSet<>(
        Comparator.comparing(UserPresenter::getFirstName)
            .thenComparing(UserPresenter::getLastName)
            .thenComparing(UserPresenter::getEmail));
    treeSet.addAll(users);
    this.users = Collections.unmodifiableSet(treeSet);
    this.size = users.size();
  }
}
