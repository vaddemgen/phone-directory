package com.epam.phonedirectory.app.applicationlayer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("errors")
@Controller
public class ErrorController {

  @GetMapping("notFound")
  public String renderErrorPage() {
    return "error/pageNotFound";
  }

  @GetMapping("/accessDenied")
  private String renderAccessDeniedPage() {
    return "error/accessDenied";
  }
}
