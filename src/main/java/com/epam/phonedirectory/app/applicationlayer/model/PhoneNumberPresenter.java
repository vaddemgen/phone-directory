package com.epam.phonedirectory.app.applicationlayer.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@SuppressWarnings("WeakerAccess") // Must be public to access it in a view.
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@EqualsAndHashCode(doNotUseGetters = true)
@ToString(doNotUseGetters = true)
public final class PhoneNumberPresenter {

  private final long id;
  private final String type;
  private final String number;
  private final PhoneCompanyPresenter phoneCompany;
}
