package com.epam.phonedirectory.app.applicationlayer.controller;

import static com.epam.phonedirectory.app.applicationlayer.controller.UserImportUtils.readUsers;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toSet;

import com.epam.phonedirectory.api.model.User;
import com.epam.phonedirectory.api.service.ImportUserService;
import com.epam.phonedirectory.app.applicationlayer.model.UserMapper;
import com.epam.phonedirectory.app.applicationlayer.model.UsersListPresenter;
import com.epam.phonedirectory.app.businesslayer.exception.InvalidUsersImportFile;
import com.epam.phonedirectory.app.businesslayer.exception.ValidationException;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Set;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Controller
@RequestMapping("import")
public class ImportController {

  private final static String PAGE_IMPORT_INDEX = "import";
  private final static String PAGE_IMPORT_RESULT = "import_results";

  private final UserMapper userMapper;
  private final ImportUserService importUserService;

  public ImportController(UserMapper userMapper, ImportUserService importUserService) {
    this.userMapper = userMapper;
    this.importUserService = importUserService;
  }

  @GetMapping
  public String show(@RequestParam(required = false) String validationError,
      ModelMap model) {
    if (nonNull(validationError)) {
      model.addAttribute("validationError", validationError);
    }
    return PAGE_IMPORT_INDEX;
  }

  @PostMapping
  public String handle(@RequestParam("file") MultipartFile file,
      ModelMap model)
      throws IOException, InvalidUsersImportFile {

    Set<User> users = readUsers(file.getInputStream());

    try {
      log.debug("IMPORT_USERS: read data: {}", users);
      UsersImportValidator.validateUsers(users);

      Stream<User> importedUsers = importUserService.importUsers(users);

      model.addAttribute("data", new UsersListPresenter(
          importedUsers
              .map(userMapper::userToUserPresenter)
              .collect(toSet())));
      return PAGE_IMPORT_RESULT;
    } catch (ValidationException e) {
      return "redirect:" + PAGE_IMPORT_INDEX + "?validationError="
          + URLEncoder.encode(e.getMessage(), StandardCharsets.UTF_8);
    }
  }
}
