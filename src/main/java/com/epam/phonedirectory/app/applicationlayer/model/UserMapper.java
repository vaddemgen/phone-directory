package com.epam.phonedirectory.app.applicationlayer.model;

import static java.util.Objects.nonNull;

import com.epam.phonedirectory.api.model.PhoneCompany;
import com.epam.phonedirectory.api.model.PhoneNumber;
import com.epam.phonedirectory.api.model.User;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

  UserPresenter userToUserPresenter(User user);

  default User createUserFormToModel(UserForm form) {
    List<PhoneNumber> phoneNumbersModels = new ArrayList<>();
    List<String> phoneTypes = form.getPhoneTypes();
    List<String> phoneNumbers = form.getPhoneNumbers();
    List<String> phoneCompanies = form.getPhoneCompanies();

    if (nonNull(phoneTypes) && nonNull(phoneNumbers) && nonNull(phoneCompanies)) {
      for (int i = 0; i < phoneTypes.size() && i < phoneNumbers.size() && i < phoneCompanies.size();
          i++) {
        if (StringUtils.isBlank(phoneTypes.get(i))
            || StringUtils.isBlank(phoneNumbers.get(i))
            || StringUtils.isBlank(phoneCompanies.get(i))) {
          continue;
        }
        phoneNumbersModels.add(
            PhoneNumber.builder()
                .id(0)
                .type(phoneTypes.get(i).trim())
                .number(phoneNumbers.get(i).trim())
                .phoneCompany(
                    PhoneCompany.builder()
                        .id(0)
                        .companyName(phoneCompanies.get(i).trim())
                        .build()
                )
                .build()
        );
      }
    }

    return User.builder()
        .firstName(form.getFirstName().trim())
        .lastName(form.getLastName().trim())
        .email(form.getEmail().trim())
        .phones(phoneNumbersModels)
        .build();
  }
}
