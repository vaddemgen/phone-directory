package com.epam.phonedirectory.app.applicationlayer.controller;

import static java.util.Objects.nonNull;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

  @GetMapping(value = "/login")
  public String show(@RequestParam(required = false) String error,
      ModelMap model) {
    if (nonNull(error)) {
      model.addAttribute("error", "Bad credentials");
    }
    return "login";
  }
}
