package com.epam.phonedirectory.app.applicationlayer.model;

import com.epam.phonedirectory.api.model.UserAccount;
import java.text.NumberFormat;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserAccountMapper {

  @Mapping(target = "accountNumber", expression = "java(toAccountNumber(model.getId()))")
  @Mapping(target = "amount", expression = "java(toAmount(model.getAmount()))")
  UserAccountPresenter toUserAccountPresenter(UserAccount model);

  default String toAccountNumber(long id) {
    return String.format("#%05d", id);
  }

  default String toAmount(double amount) {
    return NumberFormat.getCurrencyInstance()
        .format(amount);
  }
}
