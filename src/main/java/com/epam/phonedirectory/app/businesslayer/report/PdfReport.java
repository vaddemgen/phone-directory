package com.epam.phonedirectory.app.businesslayer.report;

import com.epam.phonedirectory.app.applicationlayer.model.PhoneNumberPresenter;
import com.epam.phonedirectory.app.applicationlayer.model.UserPresenter;
import com.itextpdf.io.font.constants.StandardFontFamilies;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Div;
import com.itextpdf.layout.element.List;
import com.itextpdf.layout.element.ListItem;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import java.io.IOException;
import java.io.OutputStream;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import org.jetbrains.annotations.NotNull;

public final class PdfReport {

  private PdfReport() {
  }

  public static void generatePdfReport(UsersReport report, OutputStream outputStream)
      throws IOException {
    // IText API
    PdfWriter pdfWriter = new PdfWriter(outputStream);
    PdfDocument pdf = new PdfDocument(pdfWriter);
    Document pdfDocument = new Document(pdf);

    // title
    Paragraph title = new Paragraph(report.getName());
    title.setFont(PdfFontFactory.createFont(StandardFontFamilies.HELVETICA));
    title.setFontSize(18f);
    title.setBold();
    title.setTextAlignment(TextAlignment.CENTER);
    pdfDocument.add(title);

    // vendor
    Paragraph vendor = new Paragraph("Phone Directory");
    vendor.setFontSize(12f);
    vendor.setItalic();
    pdfDocument.add(vendor);

    // content
    Table usersTable = new Table(UnitValue.createPercentArray(new float[]{4, 15, 15, 26, 40}))
        .setWidth(UnitValue.createPercentValue(100))
        .addHeaderCell("#")
        .addHeaderCell("First Name")
        .addHeaderCell("Last Name")
        .addHeaderCell("Email")
        .addHeaderCell("Phones");

    for (int i = 0; i < report.getUsers().size(); i++) {
      UserPresenter user = report.getUsers().get(i);
      usersTable.addCell(String.valueOf(i + 1));
      usersTable.addCell(user.getFirstName());
      usersTable.addCell(user.getLastName());
      usersTable.addCell(user.getEmail());
      List list = new List();
      user.getPhones()
          .stream()
          .map(p -> MessageFormat.format("{0}, {1}, {2};",
              p.getType(), p.getNumber(), p.getPhoneCompany().getCompanyName()))
          .map(ListItem::new)
          .forEach(list::add);
      usersTable.addCell(list);
    }

    usersTable.addFooterCell("#")
        .addFooterCell("First Name")
        .addFooterCell("Last Name")
        .addFooterCell("Email")
        .addFooterCell("Phones");

    pdfDocument.add(usersTable);

    // date
    Paragraph date = new Paragraph(report.getDate().format(
        DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
    date.setFontSize(12f);
    date.setItalic();
    pdfDocument.add(date);

    pdfDocument.close();
  }

  public static void generatePdfForUser(@NotNull UserPresenter user, OutputStream outputStream)
      throws IOException {
    // IText API
    PdfWriter pdfWriter = new PdfWriter(outputStream);
    PdfDocument pdf = new PdfDocument(pdfWriter);
    Document pdfDocument = new Document(pdf);

    // title
    Paragraph title = new Paragraph("User " + String.format("#%05d", user.getId()));
    title.setFont(PdfFontFactory.createFont(StandardFontFamilies.HELVETICA));
    title.setFontSize(18f);
    title.setBold();
    title.setTextAlignment(TextAlignment.CENTER);
    pdfDocument.add(title);

    // vendor
    Paragraph vendor = new Paragraph("Phone Directory");
    vendor.setFontSize(12f);
    vendor.setItalic();
    pdfDocument.add(vendor);

    // First Name
    pdfDocument.add(
        new Paragraph()
            .setFontSize(12f)
            .add(new Text("First Name: ").setBold())
            .add(user.getFirstName())
    );

    // Last Name
    pdfDocument.add(
        new Paragraph()
            .setFontSize(12f)
            .add(new Text("Last Name: ").setBold())
            .add(user.getLastName())
    );

    // Email
    pdfDocument.add(
        new Paragraph()
            .setFontSize(12f)
            .add(new Text("Email: ").setBold())
            .add(user.getEmail())
    );

    // content
    Table usersTable = new Table(UnitValue.createPercentArray(new float[]{5, 15, 35, 35}))
        .setWidth(UnitValue.createPercentValue(100))
        .setCaption(new Div().add(new Paragraph("Phones:").setBold()))
        .addHeaderCell(new Cell().setTextAlignment(TextAlignment.CENTER).add(new Paragraph("#")))
        .addHeaderCell("CODE")
        .addHeaderCell("PHONE NUMBER")
        .addHeaderCell("PHONE COMPANY");

    int i = 1;
    for (PhoneNumberPresenter phone : user.getPhones()) {
      usersTable.addCell(
          new Cell()
              .setTextAlignment(TextAlignment.CENTER)
              .add(new Paragraph(String.valueOf(i++)))
      );
      usersTable.addCell(phone.getType());
      usersTable.addCell(phone.getNumber());
      usersTable.addCell(phone.getPhoneCompany().getCompanyName());
    }

    usersTable
        .addFooterCell(new Cell().setTextAlignment(TextAlignment.CENTER).add(new Paragraph("#")))
        .addFooterCell("CODE")
        .addFooterCell("PHONE NUMBER")
        .addFooterCell("PHONE COMPANY");

    pdfDocument.add(usersTable);

    // date
    Paragraph date = new Paragraph(LocalDate.now().format(
        DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
    date.setFontSize(12f);
    date.setItalic();
    pdfDocument.add(date);

    pdfDocument.close();
  }
}
