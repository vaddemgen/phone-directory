package com.epam.phonedirectory.app.businesslayer.report;

import com.epam.phonedirectory.app.applicationlayer.model.UserPresenter;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import lombok.Builder;
import lombok.Getter;

@Getter
public final class UsersReport {

  private final String name;
  private final List<UserPresenter> users;
  private final LocalDateTime date;

  @Builder
  private UsersReport(String name, Collection<UserPresenter> users, LocalDateTime date) {
    this.name = name;
    this.users = List.copyOf(users);
    this.date = date;
  }
}
