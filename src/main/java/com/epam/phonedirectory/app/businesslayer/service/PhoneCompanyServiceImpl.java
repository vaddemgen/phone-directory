package com.epam.phonedirectory.app.businesslayer.service;

import com.epam.phonedirectory.api.model.PhoneCompany;
import com.epam.phonedirectory.api.service.PhoneCompanyService;
import com.epam.phonedirectory.app.persistencelayer.PhoneCompanyDataMapper;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public final class PhoneCompanyServiceImpl implements PhoneCompanyService {

  private final PhoneCompanyDataMapper dataMapper;

  @Override
  public PhoneCompany getPhoneCompany(long phoneCompanyId) {
    return dataMapper.getByCompanyId(phoneCompanyId)
        .orElseThrow();
  }

  @Override
  public PhoneCompany importPhoneCompany(PhoneCompany model) {
    return dataMapper.getByCompanyName(model.getCompanyName())
        .orElseGet(() -> dataMapper.addPhoneCompany(model));
  }

  @Override
  public Stream<PhoneCompany> getAll() {
    return dataMapper.getAll().stream();
  }
}
