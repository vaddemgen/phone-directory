package com.epam.phonedirectory.app.businesslayer.model;

import static java.util.Collections.emptySet;
import static java.util.Objects.nonNull;

import com.epam.phonedirectory.api.model.Role;
import com.epam.phonedirectory.api.model.User;
import com.epam.phonedirectory.api.model.UserAuthDetail;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Getter
@ToString(doNotUseGetters = true)
@EqualsAndHashCode(doNotUseGetters = true, exclude = "roles")
public final class UserAuthDetailImpl implements UserDetails, UserAuthDetail {

  private static final long serialVersionUID = 3409238195325881026L;

  @NotNull
  private final User user;

  @NotBlank
  @Size(max = 254)
  private final String username;

  @NotBlank
  @Size(max = 255)
  private final String password;

  private final boolean accountNonExpired;
  private final boolean accountNonLocked;
  private final boolean credentialsNonExpired;
  private final boolean enabled;

  @NotNull
  private final Set<Role> roles;

  @Builder
  private UserAuthDetailImpl(User user, String username, String password, boolean accountNonExpired,
      boolean accountNonLocked, boolean credentialsNonExpired, boolean enabled,
      @Nullable Collection<Role> roles) {
    this.user = user;
    this.username = username;
    this.password = password;
    this.accountNonExpired = accountNonExpired;
    this.accountNonLocked = accountNonLocked;
    this.credentialsNonExpired = credentialsNonExpired;
    this.enabled = enabled;
    this.roles = nonNull(roles) ? Set.copyOf(roles) : emptySet();
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return roles.stream()
        .map(Role::getValue)
        .map(SimpleGrantedAuthority::new)
        .collect(Collectors.toSet());
  }

  @Override
  public Stream<Role> getRoles() {
    return roles.stream();
  }

  @Override
  public UserAuthDetail withPassword(String hash) {
    return builder()
        .user(user)
        .username(username)
        .password(hash)
        .accountNonExpired(accountNonExpired)
        .accountNonLocked(accountNonLocked)
        .credentialsNonExpired(credentialsNonExpired)
        .enabled(enabled)
        .roles(roles)
        .build();
  }

  @Override
  public UserAuthDetail withRoles(List<Role> newRoles) {
    return builder()
        .user(user)
        .username(username)
        .password(password)
        .accountNonExpired(accountNonExpired)
        .accountNonLocked(accountNonLocked)
        .credentialsNonExpired(credentialsNonExpired)
        .enabled(enabled)
        .roles(newRoles)
        .build();
  }

  @Override
  public UserAuthDetail withUsername(String newUsername) {
    return builder()
        .user(user)
        .username(newUsername)
        .password(password)
        .accountNonExpired(accountNonExpired)
        .accountNonLocked(accountNonLocked)
        .credentialsNonExpired(credentialsNonExpired)
        .enabled(enabled)
        .roles(roles)
        .build();
  }
}
