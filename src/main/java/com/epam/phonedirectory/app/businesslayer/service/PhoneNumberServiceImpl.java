package com.epam.phonedirectory.app.businesslayer.service;

import com.epam.phonedirectory.api.model.PhoneCompany;
import com.epam.phonedirectory.api.model.PhoneNumber;
import com.epam.phonedirectory.api.model.User;
import com.epam.phonedirectory.api.service.PhoneCompanyService;
import com.epam.phonedirectory.api.service.PhoneNumberService;
import com.epam.phonedirectory.api.service.UserAccountService;
import com.epam.phonedirectory.app.persistencelayer.PhoneNumberDataMapper;
import com.epam.phonedirectory.app.persistencelayer.UserDataMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class PhoneNumberServiceImpl implements PhoneNumberService {

  private final PhoneNumberDataMapper dataMapper;
  private final PhoneCompanyService phoneCompanyService;
  private final UserAccountService userAccountService;
  private final UserDataMapper userDataMapper;

  private static PhoneNumber mergePhoneNumberModel(PhoneNumber from, PhoneNumber to) {
    return to.withType(from.getType())
        .withPhoneCompany(from.getPhoneCompany());
  }

  @Transactional
  @Override
  public PhoneNumber importPhoneNumber(PhoneNumber rawModel, User owner) {
    // First, we're importing the phone company.
    PhoneCompany phoneCompany = phoneCompanyService.importPhoneCompany(rawModel.getPhoneCompany());

    // Applying changes.
    PhoneNumber model = rawModel.withPhoneCompany(phoneCompany);

    // Importing the phone number.
    return dataMapper.getByNumber(model.getNumber())
        .map(storedModel -> mergePhoneNumberModel(model, storedModel))
        .map(mergedModel -> updatePhoneNumber(mergedModel, owner))
        .orElseGet(() -> createPhoneNumber(model, owner));
  }

  private PhoneNumber createPhoneNumber(PhoneNumber model, User user) {
    PhoneNumber savedModel = dataMapper.addPhoneNumber(model, user);
    userAccountService.createDefaultUserAccount(user, savedModel);
    return savedModel;
  }

  private PhoneNumber updatePhoneNumber(PhoneNumber model, User owner) {
    if (!isPhoneNumberAssignedToUser(model, owner)) {
      assignPhoneNumber(model, owner);
    }
    return dataMapper.updatePhoneNumber(model);
  }

  private void assignPhoneNumber(PhoneNumber phoneNumber, User user) {
    userAccountService.deleteUserAccount(phoneNumber);
    dataMapper.assignPhoneNumber(phoneNumber, user);
    userAccountService.createDefaultUserAccount(user, phoneNumber);
  }

  private boolean isPhoneNumberAssignedToUser(PhoneNumber model, User owner) {
    return userDataMapper.getByUserId(owner.getId())
        .map(user -> user.getPhones()
            .map(PhoneNumber::getNumber)
            .anyMatch(number -> model.getNumber().equals(number))
        ).orElse(false);
  }
}
