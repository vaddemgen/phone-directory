package com.epam.phonedirectory.app.businesslayer.service;

import com.epam.phonedirectory.api.exception.NotEnoughMoneyException;
import com.epam.phonedirectory.api.exception.UserAccountNotFoundException;
import com.epam.phonedirectory.api.model.PhoneNumber;
import com.epam.phonedirectory.api.model.User;
import com.epam.phonedirectory.api.model.UserAccount;
import com.epam.phonedirectory.api.service.PhoneCompanyService;
import com.epam.phonedirectory.api.service.UserAccountService;
import com.epam.phonedirectory.app.persistencelayer.UserAccountDataMapper;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
@Slf4j
public class UserAccountServiceImpl implements UserAccountService {

  private static final long DEFAULT_AMOUNT = 0;
  private static final double COMMISSION_AMOUNT = 4.99;

  private final PhoneCompanyService phoneCompanyService;
  private final UserAccountDataMapper dataMapper;

  @Transactional
  @Override
  public void createDefaultUserAccount(User user, PhoneNumber number) {
    UserAccount userAccount = UserAccount.builder()
        .user(user)
        .phoneNumber(number)
        .operator(number.getPhoneCompany())
        .amount(DEFAULT_AMOUNT)
        .build();

    log.trace("USER_ACCOUNT_CREATE: {}", userAccount);

    dataMapper.addUserAccount(userAccount);
  }

  @Override
  public Stream<UserAccount> getAccounts(User user) {
    return dataMapper.getAllByUserId(user.getId());
  }

  @Override
  public void deleteUserAccount(PhoneNumber phoneNumber) {
    dataMapper.getByPhoneNumber(phoneNumber.getNumber())
        .ifPresent(dataMapper::removeUserAccount);
  }

  @Transactional(rollbackFor = Throwable.class)
  @Override
  public void topUpAccount(User user, long accountId, double money)
      throws UserAccountNotFoundException {
    UserAccount userAccount = dataMapper.getByUserIdAndAccountId(user.getId(), accountId)
        .orElseThrow(() -> new UserAccountNotFoundException(accountId));
    dataMapper.updateUserAccount(userAccount.withAmount(userAccount.getAmount() + money));
  }

  @Transactional(rollbackFor = Throwable.class)
  @Override
  public void changeOperator(User user, long accountId, long newOperatorId)
      throws NotEnoughMoneyException, UserAccountNotFoundException {
    UserAccount userAccount = dataMapper.getByUserIdAndAccountId(user.getId(), accountId)
        .orElseThrow(() -> new UserAccountNotFoundException(accountId));

    if (userAccount.getAmount() < COMMISSION_AMOUNT) {
      throw new NotEnoughMoneyException(userAccount, COMMISSION_AMOUNT);
    }

    dataMapper.updateUserAccount(
        userAccount.withAmount(userAccount.getAmount() - COMMISSION_AMOUNT)
            .withOperator(phoneCompanyService.getPhoneCompany(newOperatorId))
    );
  }
}
