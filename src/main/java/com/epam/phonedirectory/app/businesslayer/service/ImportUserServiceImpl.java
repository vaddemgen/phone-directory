package com.epam.phonedirectory.app.businesslayer.service;

import static java.util.stream.Collectors.toList;

import com.epam.phonedirectory.api.model.PhoneCompany;
import com.epam.phonedirectory.api.model.PhoneNumber;
import com.epam.phonedirectory.api.model.User;
import com.epam.phonedirectory.api.service.ImportUserService;
import com.epam.phonedirectory.api.service.UserService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public final class ImportUserServiceImpl implements ImportUserService {

  private final UserService userService;

  @Transactional
  @Override
  public Stream<User> importUsers(Collection<User> userCollection) {
    // Preparing data fot the import process.
    List<User> usersToImport = userCollection.stream()
        .map(this::handleUserData)
        .collect(toList());

    // Importing the users batch.
    List<User> importedUsers = new ArrayList<>(usersToImport.size());
    for (User user : usersToImport) {
      importedUsers.add(userService.importUser(user));
    }

    return importedUsers.stream();
  }

  private User handleUserData(User user) {
    return User.builder()
        .id(user.getId())
        .firstName(StringUtils.trim(user.getFirstName()))
        .lastName(StringUtils.trim(user.getLastName()))
        .email(user.getEmail())
        .phones(
            user.getPhones()
                .map(this::handlePhoneNumberData)
                .collect(Collectors.toSet())
        ).build();
  }

  private PhoneNumber handlePhoneNumberData(PhoneNumber phoneNumber) {
    return PhoneNumber.builder()
        .id(phoneNumber.getId())
        .type(StringUtils.trim(phoneNumber.getType()))
        .number(RegExUtils.replaceAll(phoneNumber.getNumber(), "[^0-9+]", ""))
        .phoneCompany(
            PhoneCompany.builder()
                .companyName(StringUtils.trim(phoneNumber.getPhoneCompany().getCompanyName()))
                .build())
        .build();
  }
}
