package com.epam.phonedirectory.app.businesslayer.service;

import com.epam.phonedirectory.api.service.UserAuthDetailService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component("userDetailsService")
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

  private final UserAuthDetailService userAuthDetailService;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    return (UserDetails) userAuthDetailService.getUserAuthDetail(username);
  }
}
