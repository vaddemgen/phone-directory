package com.epam.phonedirectory.app.businesslayer.exception;

import lombok.Getter;

@Getter
public class ValidationException extends Exception {

  public ValidationException(String message) {
    super(message);
  }
}
