package com.epam.phonedirectory.app.businesslayer.report;

import java.text.MessageFormat;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.AbstractView;

@Component("usersReportView")
public final class UsersReportPdfView extends AbstractView {

  @Override
  protected void renderMergedOutputModel(Map<String, Object> model,
      @NotNull HttpServletRequest request, HttpServletResponse response) throws Exception {
    UsersReport report = (UsersReport) model.get("report");
    response.setHeader("Content-Disposition", MessageFormat.format(
        "attachment; filename={0}_{1}.pdf",
        report.getName(),
        report.getDate().format(DateTimeFormatter.ISO_DATE)));

    ServletOutputStream outputStream = response.getOutputStream();
    PdfReport.generatePdfReport(report, outputStream);
  }
}
