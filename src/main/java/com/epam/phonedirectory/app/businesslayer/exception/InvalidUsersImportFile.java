package com.epam.phonedirectory.app.businesslayer.exception;

import com.google.gson.JsonParseException;

public final class InvalidUsersImportFile extends Exception {

  public InvalidUsersImportFile(JsonParseException e) {
    super(e.getMessage(), e);
  }
}
