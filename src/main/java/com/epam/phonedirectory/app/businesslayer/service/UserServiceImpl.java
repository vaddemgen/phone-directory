package com.epam.phonedirectory.app.businesslayer.service;

import com.epam.phonedirectory.api.exception.ModelNotFoundException;
import com.epam.phonedirectory.api.model.PhoneNumber;
import com.epam.phonedirectory.api.model.Role;
import com.epam.phonedirectory.api.model.User;
import com.epam.phonedirectory.api.model.UserAuthDetail;
import com.epam.phonedirectory.api.service.PhoneNumberService;
import com.epam.phonedirectory.api.service.UserAuthDetailService;
import com.epam.phonedirectory.api.service.UserService;
import com.epam.phonedirectory.app.persistencelayer.UserDataMapper;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

  private final UserDataMapper dataMapper;
  private final UserAuthDetailService userAuthDetailService;
  private final PhoneNumberService phoneNumberService;

  @Override
  public Stream<User> getUsers() {
    return dataMapper.getUsers().stream();
  }

  @Override
  public User getUser(long userId) throws ModelNotFoundException {
    return dataMapper.getByUserId(userId)
        .orElseThrow(() -> new ModelNotFoundException(MessageFormat.format(
            "User with id ''{0}'' not found.", userId)));
  }

  @Override
  public void deleteUser(long userId) {
    dataMapper.removeUserById(userId);
  }

  @Transactional
  @Override
  public User importUser(User user) {
    User importedUser = dataMapper.getByEmail(user.getEmail())
        .map(storedModel -> mergeUserModel(user, storedModel))
        .map(dataMapper::updateUser)
        .orElseGet(() -> createUser(user));
    return importedUser.withPhones(importPhones(user.withId(importedUser.getId())));
  }

  @Transactional
  @Override
  public User createUser(User user, String password, List<Role> roles) {
    User createdUser = dataMapper.addUser(user);
    userAuthDetailService.createUserAuthDetail(createdUser, password, roles);
    return createdUser.withPhones(importPhones(user.withId(createdUser.getId())));
  }

  @Override
  public Page<User> getUsers(Pageable pageable) {
    return dataMapper.getUsers(pageable);
  }

  private Collection<PhoneNumber> importPhones(User user) {
    List<PhoneNumber> phoneNumbers = user.getPhones().collect(Collectors.toList());
    var result = new ArrayList<PhoneNumber>(phoneNumbers.size());
    for (PhoneNumber phoneNumber : phoneNumbers) {
      result.add(phoneNumberService.importPhoneNumber(phoneNumber, user));
    }
    return result;
  }

  private User createUser(User user) {
    User createdUser = dataMapper.addUser(user);
    userAuthDetailService.createUserAuthDetail(createdUser);
    return createdUser;
  }

  private User mergeUserModel(User from, User to) {
    return to.withFirstName(from.getFirstName())
        .withLastName(from.getLastName())
        .withEmail(from.getEmail());
  }

  @Transactional
  @Override
  public User updateUser(User user, @Nullable String password, @Nullable List<Role> roles) {

    // Updating User model.
    User updatedUser = dataMapper.updateUser(user);

    // Updating authentication details.
    UserAuthDetail userAuthDetail = userAuthDetailService.getUserAuthDetail(user.getId());
    userAuthDetailService
        .updateUserAuthDetail(updatedUser, updatedUser.getEmail(), password, roles);

    importPhones(user.withId(updatedUser.getId()));
    return updatedUser;
  }
}
