package com.epam.phonedirectory.app.businesslayer.service;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

import com.epam.phonedirectory.api.model.Role;
import com.epam.phonedirectory.api.model.User;
import com.epam.phonedirectory.api.model.UserAuthDetail;
import com.epam.phonedirectory.api.service.UserAuthDetailService;
import com.epam.phonedirectory.app.businesslayer.model.UserAuthDetailImpl;
import com.epam.phonedirectory.app.persistencelayer.UserAuthDetailDataMapper;
import java.text.MessageFormat;
import java.util.List;
import java.util.Random;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public final class UserAuthDetailServiceImpl implements UserAuthDetailService {

  public static final int RANDOM_PASSWORD_LENGTH = 18;

  private final PasswordEncoder passwordEncoder;
  private final UserAuthDetailDataMapper dataMapper;

  private static String generateRandomPassword() {
    String saltChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    StringBuilder salt = new StringBuilder();
    Random rnd = new Random();
    while (salt.length() < RANDOM_PASSWORD_LENGTH) { // length of the random string.
      int index = (int) (rnd.nextFloat() * saltChars.length());
      salt.append(saltChars.charAt(index));
    }
    return salt.toString();
  }

  @Override
  public UserAuthDetail createUserAuthDetail(User user, String password, List<Role> roles) {
    return createUserAuthDetail(fillCredentials(user, password, roles));
  }

  @Override
  public void createUserAuthDetail(User user) {
    createUserAuthDetail(fillCredentials(user));
  }

  @Override
  public void updateUserAuthDetail(User user, String password, List<Role> roles) {
    // Getting the user auth details model.
    UserAuthDetail model = dataMapper.getByUserId(user.getId()).orElseThrow();

    // Updating model data.
    UserAuthDetail changedModel = model.withPassword(
        nonNull(password) ? passwordEncoder.encode(password) : model.getPassword()
    ).withRoles(roles);

    // Committing changes.
    dataMapper.updateUserAuthDetail(changedModel);
  }

  @Override
  public UserAuthDetail getUserAuthDetail(String email) {
    return dataMapper.getByEmail(email)
        .orElseThrow(() -> new UsernameNotFoundException(MessageFormat.format(
            "User with login ''{0}'' not found", email)));
  }

  @Override
  public UserAuthDetail getUserAuthDetail(long userId) {
    return dataMapper.getByUserId(userId)
        .orElseThrow(() -> new UsernameNotFoundException(MessageFormat.format(
            "User with id ''{0}'' not found", userId)));
  }

  @Override
  public void updateUserAuthDetail(User user, String username, @Nullable String password,
      @Nullable List<Role> roles) {
    // Getting the user auth details model.
    UserAuthDetail model = dataMapper.getByUserId(user.getId()).orElseThrow();

    // Updating model data.
    UserAuthDetail updatedModel = model
        .withUsername(username)
        .withPassword(nonNull(password) ? passwordEncoder.encode(password) : model.getPassword())
        .withRoles(nonNull(roles) ? roles : model.getRoles().collect(toList()));

    // Committing changes.
    dataMapper.updateUserAuthDetail(updatedModel);
  }

  private UserAuthDetail createUserAuthDetail(UserAuthDetail userAuthDetail) {
    return dataMapper.addUserAuthDetail(userAuthDetail);
  }

  public UserAuthDetail fillCredentials(User user) {
    return fillCredentials(user, generateRandomPassword(), List.of(Role.USER));
  }

  private UserAuthDetail fillCredentials(User user, String password, List<Role> roles) {
    return UserAuthDetailImpl.builder()
        .user(user)
        .username(user.getEmail())
        .password(nonNull(password) ? passwordEncoder.encode(password) : null)
        .roles(roles)
        .accountNonExpired(true)
        .accountNonLocked(true)
        .credentialsNonExpired(true)
        .enabled(true)
        .build();
  }
}
