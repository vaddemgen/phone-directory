package com.epam.phonedirectory.app.api;

import com.epam.phonedirectory.app.applicationlayer.model.UserPresenter;
import com.epam.phonedirectory.app.businesslayer.report.PdfReport;
import java.io.IOException;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.stereotype.Component;

@Component
public final class PdfUserHttpMessageConverter extends AbstractHttpMessageConverter<UserPresenter> {

  public PdfUserHttpMessageConverter() {
    super(MediaType.APPLICATION_PDF);
  }

  @Override
  protected boolean supports(@NotNull Class<?> clazz) {
    return clazz.isAssignableFrom(UserPresenter.class);
  }

  @NotNull
  @Override
  protected UserPresenter readInternal(@NotNull Class<? extends UserPresenter> clazz,
      @NotNull HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
    throw new HttpMessageNotReadableException("Not supported", inputMessage);
  }

  @Override
  protected void writeInternal(@NotNull UserPresenter user, HttpOutputMessage outputMessage)
      throws IOException, HttpMessageNotWritableException {
    PdfReport.generatePdfForUser(user, outputMessage.getBody());
  }

  @Override
  public boolean canRead(@NotNull Class<?> clazz, MediaType mediaType) {
    return false;
  }
}
