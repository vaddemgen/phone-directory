package com.epam.phonedirectory.app.api.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(doNotUseGetters = true)
public final class UserDto {

  @NotBlank
  @Pattern(regexp = "[a-zA-Z]+")
  @Size(max = 50)
  private final String firstName;

  @NotBlank
  @Pattern(regexp = "[a-zA-Z]+")
  @Size(max = 50)
  private final String lastName;

  @NotBlank
  @Email
  @Size(max = 254)
  private final String email;

  @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
  public UserDto(
      @JsonProperty("firstName") String firstName,
      @JsonProperty("lastName") String lastName,
      @JsonProperty("email") String email
  ) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
  }
}
