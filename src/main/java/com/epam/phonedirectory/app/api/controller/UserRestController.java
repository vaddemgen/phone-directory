package com.epam.phonedirectory.app.api.controller;

import com.epam.phonedirectory.api.exception.ModelNotFoundException;
import com.epam.phonedirectory.api.model.ErrorMessage;
import com.epam.phonedirectory.api.model.User;
import com.epam.phonedirectory.api.service.UserService;
import com.epam.phonedirectory.app.api.model.RestUserForm;
import com.epam.phonedirectory.app.api.model.UserDto;
import com.epam.phonedirectory.app.applicationlayer.model.UserMapper;
import com.epam.phonedirectory.app.applicationlayer.model.UserPresenter;
import java.net.URI;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("api/v1/users")
@RestController
@AllArgsConstructor
@Slf4j
public class UserRestController {

  private final UserService userService;
  private final UserMapper userMapper;

  @GetMapping("{userId}")
  public UserPresenter getUser(@PathVariable long userId) throws ModelNotFoundException {
    return userMapper.userToUserPresenter(userService.getUser(userId));
  }

  @PostMapping
  public ResponseEntity<UserPresenter> createUser(@RequestBody @Valid RestUserForm form) {
    log.trace("USER_CREATE: Entering with the user {}", form);

    // Extracting a user model from the request.
    User userToCreate = userMapper.createUserFormToModel(form);
    log.trace("USER_CREATE: Mapped user from the request {}", userToCreate);

    // Creating a user.
    User createdUser = userService.createUser(userToCreate, form.getPassword(), form.getRoles());
    log.trace("USER_CREATE: Created the user {}", createdUser);

    // Converting to a presenter.
    return ResponseEntity.created(URI.create("/users/" + createdUser.getId()))
        .body(userMapper.userToUserPresenter(createdUser));
  }

  @PutMapping("{userId}")
  public UserPresenter updateUser(@PathVariable long userId, @RequestBody @Valid UserDto userDto) {
    log.trace("USER_UPDATE: Entering with the user {}", userDto);

    // Extracting a user model from the request.
    User userToCreate = User.builder()
        .id(userId)
        .firstName(userDto.getFirstName())
        .lastName(userDto.getLastName())
        .email(userDto.getEmail())
        .build();
    log.trace("USER_UPDATE: Mapped user from the request {}", userToCreate);

    // Updating a user.
    User updatedUser = userService.updateUser(userToCreate, null, null);
    log.trace("USER_UPDATE: Created the user {}", updatedUser);

    return userMapper.userToUserPresenter(updatedUser);
  }

  @GetMapping
  public Page<UserPresenter> getUsers(@PageableDefault Pageable pageable) {
    return userService.getUsers(pageable)
        .map(userMapper::userToUserPresenter);
  }

  @ExceptionHandler(ModelNotFoundException.class)
  public ResponseEntity<ErrorMessage> handleModelNotFoundException(ModelNotFoundException e) {
    log.trace("MODEL_NOT_FOUND", e);
    return ResponseEntity.status(HttpStatus.NOT_FOUND)
        .body(ErrorMessage.builder().error(e.getMessage()).build());
  }

  @ExceptionHandler(HttpMessageNotReadableException.class)
  public ResponseEntity<ErrorMessage> handleHttpMessageNotReadableException(
      HttpMessageNotReadableException e) {
    log.trace("VALIDATION_ERROR", e);
    return ResponseEntity.badRequest()
        .body(ErrorMessage.builder().error(e.getMessage()).build());
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<ErrorMessage> handleInternalServerError(Exception e) {
    log.error("INTERNAL_ERROR", e);
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .body(ErrorMessage.builder().error(e.getMessage()).build());
  }
}
