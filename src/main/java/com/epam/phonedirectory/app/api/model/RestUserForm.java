package com.epam.phonedirectory.app.api.model;

import com.epam.phonedirectory.api.model.Role;
import com.epam.phonedirectory.app.applicationlayer.model.UserForm;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.ToString;
import org.jetbrains.annotations.Nullable;

@ToString(doNotUseGetters = true, callSuper = true)
public final class RestUserForm extends UserForm {

  @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
  public RestUserForm(
      @JsonProperty("firstName") String firstName,
      @JsonProperty("lastName") String lastName,
      @JsonProperty("email") String email,
      @JsonProperty("password") String password,
      @JsonProperty("confirmPassword") String confirmPassword,
      @JsonProperty("roles") @Nullable List<Role> roles) {
    super(firstName, lastName, email, password, confirmPassword, roles, null, null, null);
  }
}
