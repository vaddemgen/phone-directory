package com.epam.phonedirectory.app.api;

import com.epam.phonedirectory.app.applicationlayer.model.UserPresenter;
import com.epam.phonedirectory.app.businesslayer.report.PdfReport;
import com.epam.phonedirectory.app.businesslayer.report.UsersReport;
import java.io.IOException;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractGenericHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Component;

@Component
public final class PdfUsersHttpMessageConverter extends
    AbstractGenericHttpMessageConverter<Page<UserPresenter>> {

  public PdfUsersHttpMessageConverter() {
    super(MediaType.APPLICATION_PDF);
  }

  @Override
  protected void writeInternal(@NotNull Page<UserPresenter> page, Type type,
      HttpOutputMessage outputMessage)
      throws IOException {
    PdfReport.generatePdfReport(
        UsersReport.builder()
            .name("Users")
            .date(LocalDateTime.now())
            .users(page.toList())
            .build(),
        outputMessage.getBody()
    );
  }

  @NotNull
  @Override
  protected Page<UserPresenter> readInternal(@NotNull Class<? extends Page<UserPresenter>> clazz,
      @NotNull HttpInputMessage inputMessage)
      throws HttpMessageNotReadableException {
    throw new HttpMessageNotReadableException("Not Supported", inputMessage);
  }

  @NotNull
  @Override
  public Page<UserPresenter> read(@NotNull Type type, Class<?> contextClass,
      @NotNull HttpInputMessage inputMessage)
      throws HttpMessageNotReadableException {
    throw new HttpMessageNotReadableException("Not Supported", inputMessage);
  }

  @Override
  public boolean canRead(@NotNull Class<?> clazz, MediaType mediaType) {
    return false;
  }
}