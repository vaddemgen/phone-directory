<#include "base.ftl">
<#macro page_head>
  <title>Profile</title>
</#macro>

<#macro page_body>
<div class="row">
  <div class="col-sm-12">
    <h4>Profile</h4>
  </div>
</div>
<#if errorMessage?? >
<div class="row">
  <div class="col-sm-12">
    <div class="alert alert-danger" role="alert">${errorMessage}</div>
  </div>
</div>
</#if>
<#if successMessage?? >
<div class="row">
  <div class="col-sm-12">
    <div class="alert alert-success" role="alert">${successMessage}</div>
  </div>
</div>
</#if>
<div class="row">
  <div class="col-md-4">
    <div class="card mb-4 shadow-sm">
      <div class="card-header">
        <h4 class="my-0 font-weight-normal text-center">General</h4>
      </div>
      <div class="card-body">
        <ul>
          <li class="align-middle">Name: ${user.firstName} ${user.lastName}</li>
          <li class="align-middle">Email: ${user.email}</li>
          <li class="align-middle">Phones:
            <ul>
              <#list user.phones as phone>
              <li>${phone.type}, ${phone.number}, ${phone.phoneCompany.companyName}</li>
            </#list>
        </ul>
      </div>
    </div>
  </div>
  <#list accounts as account>
    <div class="col-md-4">
      <div class="card mb-4 shadow-sm">
        <div class="card-header">
          <h4 class="my-0 font-weight-normal">Account <b>${account.accountNumber}</b></h4>
        </div>
        <div class="card-body">
          <h1 class="card-title pricing-card-title">${account.amount}</h1>
          <ul class="list-unstyled mt-3 mb-4">
            <li>Code: <i>${account.phoneNumber.type}</i></li>
            <li>Phone Number: <i>${account.phoneNumber.number}</i></li>
            <li>
              Operator:
              <i>${account.operator.companyName}</i>
              <a href="#"
                  data-toggle="modal"
                  data-target="#changeOperator${account.id}">change</a>
            </li>
          </ul>
          <form action="/userAccounts/${account.id}/topUp" method="post">
            <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"/>
            <button type="submit" class="btn btn-lg btn-block btn-primary">Top up</button>
          </form>
        </div>
      </div>
      <!-- Modal -->
      <div class="modal fade" id="changeOperator${account.id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <form class="modal-content" method="post" action="/userAccounts/${account.id}/changeOperator">
            <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"/>
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle${account.id}">Change mobile operator</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                  <label for="accountNumber${account.id}">Account Number</label>
                  <input type="text" class="form-control" id="accountNumber${account.id}" disabled value="${account.accountNumber}">
                </div>
                <div class="form-group">
                  <label for="operator${account.id}">Current operator</label>
                  <input type="text" class="form-control" id="operator${account.id}" disabled value="${account.operator.companyName}">
                </div>
                <div class="form-group">
                  <label for="newOperator${account.id}">Change to</label>
                  <select class="form-control" id="newOperator${account.id}" name="operatorId">
                    <#list operators?filter(o -> o.id != account.operator.id) as operator>
                      <option value="${operator.id}">${operator.companyName}</option>
                    </#list>
                  </select>
                </div>
                <b><i>It costs only $4.99!</i></b>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </#list>
</div>
</#macro>

<@display_page/>
