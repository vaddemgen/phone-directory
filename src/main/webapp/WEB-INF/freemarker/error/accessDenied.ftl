<#include "../parent.ftl">
<#macro page_head>
  <title>Access Denied</title>
</#macro>

<#macro page_body>
  <div class="row">
    <div class="col-sm-12" style="margin-top: 20px">
      <h1>403 Forbidden</h1>
      <div class="alert alert-warning" role="alert">
        You are not allowed to enter here!<br>
        The page you're trying to access has restricted access.<br>
      </div>
      <a class="btn btn-secondary" href="/" role="button">Go Back</a>
    </div>
  </div>
</#macro>

<@display_page/>
