<#include "../base.ftl">
<#macro page_head>
  <title>Error</title>
</#macro>

<#macro page_body>
  <div class="row">
    <div class="col-sm-12">
      <h4>Error:</h4>
      <div class="alert alert-danger" role="alert">${ex.getMessage()}</div>
      <a class="btn btn-secondary" href="/" role="button">Back</a>
    </div>
  </div>
</#macro>

<@display_page/>
