<#include "../base.ftl">
<#macro page_head>
  <title>Import error</title>
</#macro>

<#macro page_body>
  <div class="row">
    <div class="col-sm-12">
      <h4>Import error:</h4>
      <div class="alert alert-danger" role="alert">${ex.getMessage()}</div>
      <a class="btn btn-secondary" href="/import" role="button">Back</a>
    </div>
  </div>
</#macro>

<@display_page/>
