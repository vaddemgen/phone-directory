<#include "../parent.ftl">
<#macro page_head>
  <title>Page Not Found</title>
</#macro>

<#macro page_body>
  <div class="row">
    <div class="col-sm-12">
      <h4>Error:</h4>
      <div class="alert alert-danger" role="alert">Page Not Found</div>
    </div>
  </div>
</#macro>

<@display_page/>
