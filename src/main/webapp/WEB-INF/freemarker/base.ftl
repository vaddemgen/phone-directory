<#include "parent.ftl">
<#import "/spring.ftl" as spring/>

<#assign security=JspTaglibs["http://www.springframework.org/security/tags"] />

<#macro page_header>
<header>
  <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <a href="/users" class="text-dark">
      <h5 class="my-0 font-weight-normal">Telephone Directory</h5>
    </a>
    <div class="mr-md-3"></div>
    <nav class="my-2 my-md-0 mr-md-3 mr-md-auto">
      <@security.authorize access="hasRole('BOOKING_MANAGER')">
      <a class="p-2 text-dark" href="/users">Users</a>
      <a class="p-2 text-dark" href="/import">Import</a>
      </@security.authorize>
      <a class="p-2 text-dark" href="/profile">Profile</a>
    </nav>
    <form class="my-0" action="/logout" method="post">
      <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"/>
      <button type="submit" class="btn btn-light">Logout</button>
    </form>
  </div>
</header>
</#macro>