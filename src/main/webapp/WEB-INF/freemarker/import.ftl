<#include "base.ftl">
<#macro page_head>
  <title>Import Users</title>
</#macro>

<#macro page_body>
  <div class="row">
    <div class="col-sm-12">
      <h4>Import users</h4>
        <#if validationError?? >
          <div class="alert alert-warning" role="alert">${validationError}</div>
        </#if>
      <form method="post" action="/import" enctype="multipart/form-data">
        <div class="form-group">
          <label for="import-users">Attach file in JSON format</label>
          <input type="file"
                 id="import-users"
                 name="file"
                 accept="application/json"
                 class="form-control-file"
                 required>
        </div>
        <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"/>
        <div class="form-row">
          <div class="form-group col-xs-12 col-sm-2">
            <a class="btn btn-secondary" href="/users" role="button">Back</a>
          </div>
          <div class="form-group col-xs-12 col-sm-10">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</#macro>

<@display_page/>
