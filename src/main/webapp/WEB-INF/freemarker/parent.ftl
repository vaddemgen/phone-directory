<#macro page_head>
  <title>Telephone Directory</title>
</#macro>

<#macro page_header>
</#macro>

<#macro page_body>
  <h1>Basic Page</h1>
  <p>This is the body of the page!</p>
</#macro>

<#macro display_page>
  <!doctype html>
  <html lang="en">
  <head>
    <@page_head/>
    <link rel="stylesheet" href="/resources/css/main.min.css">
  </head>
  <body>
  <@page_header/>
  <main role="main">
    <div class="container">
        <@page_body/>
    </div>
  </main>
  <script type="text/javascript" src="/resources/js/app.min.js"></script>
  </body>
  </html>
</#macro>