<#include "parent.ftl">
<#macro page_head>
<title>Sign In | Phone Directory</title>
</#macro>

<#macro page_body>
<div class="row">
  <div class="col-sm-12 col-md-6 offset-md-3 col-lg-4 offset-lg-4">
    <form action="/login" class="form-signin" method="post">
      <#if error?? >
        <div class="alert alert-danger" role="alert">${error}</div>
      </#if>
      <h2 class="form-signin-heading">Please sign in</h2>
      <div class="form-group">
        <label for="username">Email address</label>
        <input aria-describedby="emailHelp"
               class="form-control"
               id="username"
               name="username"
               required
               type="email">
        <small class="form-text text-muted" id="emailHelp">We'll never share your email with anyone
          else.</small>
      </div>
      <div class="form-group">
        <label for="password">Password</label>
        <input class="form-control"
               id="password"
               name="password"
               required
               type="password">
      </div>
      <div class="form-group form-check">
        <input class="form-check-input" id="remember-me" name="remember-me" type="checkbox">
        <label class="form-check-label" for="remember-me">Remember me</label>
      </div>
      <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"/>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign In</button>
    </form>
  </div>
</div>
</#macro>

<@display_page/>
