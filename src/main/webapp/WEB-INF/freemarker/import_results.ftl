<#include "base.ftl">
<#macro page_head>
  <title>Import Results</title>
</#macro>

<#macro page_body>
  <div class="row">
    <div class="col-sm-12">
      <h4>Imported users</h4>
      <table class="table table-hover">
        <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">First Name</th>
          <th scope="col">Last Name</th>
          <th scope="col">Email</th>
          <th scope="col">Phones</th>
        </tr>
        </thead>
        <tbody>
        <#assign count = 1>
        <#list data.users as user>
          <tr>
            <th scope="row" class="align-middle">
                ${count}
                <#assign count++>
            </th>
            <td class="align-middle">${user.firstName}</td>
            <td class="align-middle">${user.lastName}</td>
            <td class="align-middle">${user.email}</td>
            <td class="align-middle">
                <#list user.phones as phone>
                    ${phone.type}, ${phone.number}, ${phone.phoneCompany.companyName}<br>
                </#list>
            </td>
          </tr>
        </#list>
        </tbody>
        <tfoot>
          <tr>
            <th scope="col">#</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Email</th>
            <th scope="col">Phones</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <a class="btn btn-secondary" href="/import" role="button">Back</a>
    </div>
    <div class="col-sm-6 text-right">
      <a class="btn btn-primary" href="/users" role="button">Go to Users</a>
    </div>
  </div>
</#macro>

<@display_page/>
