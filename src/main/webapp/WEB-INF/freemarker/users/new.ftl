<#include "../base.ftl">
<#macro page_head>
  <title>Add New User</title>
</#macro>

<#macro page_body>
  <div class="row">
    <div class="col-sm-12 col-md-8 offset-md-2">
      <h4>Create a user. Please fill out the form:</h4>
        <#if (validationError)?? >
          <div class="alert alert-warning" role="alert">${validationError}</div>
        </#if>
      <form method="post" action="/users/new" class="edit-class-form">
        <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"/>
        <div class="form-row">
          <div class="form-group col-sm-12 col-md-6">
            <label for="firstName">First Name</label>
            <input type="text"
                   class="form-control"
                   id="firstName"
                   name="firstName"
                   value='${(user.firstName)!""}'
                   placeholder="Enter first name..."
                   required
                   maxlength="50"
                   pattern="[A-Za-z]+">
          </div>
          <div class="form-group col-sm-12 col-md-6">
            <label for="lastName">Last Name</label>
            <input type="text"
                   class="form-control"
                   id="lastName"
                   name="lastName"
                   value='${(user.lastName)!""}'
                   placeholder="Enter last name..."
                   required
                   maxlength="50"
                   pattern="[A-Za-z]+">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-sm-12 col-md-6">
            <label for="email">Email</label>
            <input type="email"
                   class="form-control"
                   id="email"
                   name="email"
                   value='${(user.email)!""}'
                   placeholder="Enter e-mail address..."
                   maxlength="254"
                   required>
          </div>
          <div class="form-group col-sm-12 col-md-6">
            <label for="roles">Roles</label>
            <select multiple class="form-control" name="roles" id="roles">
              <option value="USER"
                <#if (roles)?? >
                  ${roles?seq_contains("USER")?string("selected", "")}
                </#if>
              >User</option>
              <option value="BOOKING_MANAGER"
                <#if (roles)?? >
                  ${roles?seq_contains("BOOKING_MANAGER")?string("selected", "")}
                </#if>
              >Booking manager</option>
            </select>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-sm-12 col-md-6">
            <label for="password">Password</label>
            <input type="password"
                   class="form-control"
                   id="password"
                   name="password"
                   placeholder="Enter password..."
                   maxlength="50"
                   minlength="4"
                   required>
          </div>
          <div class="form-group col-sm-12 col-md-6">
            <label for="confirmPassword">Confirm password</label>
            <input type="password"
                   class="form-control"
                   id="confirmPassword"
                   name="confirmPassword"
                   maxlength="50"
                   minlength="4"
                   placeholder="Confirm password..."
                   required>
          </div>
        </div>
        <div class="addresses">
          <#if (user)??>
            <#list user.phones as phone>
              <div class="form-row" data-address-id="${phone.id}">
              <div class="form-group col-sm-12 col-md-2">
                <label for="phone_type_${phone.id}">Phone type</label>
                <input type="text"
                       id="phone_type_${phone.id}"
                       class="form-control"
                       name="phoneTypes"
                       placeholder="Enter phone type: home, work, etc..."
                       required
                       value="${phone.type}"
                       maxlength="10">
              </div>
              <div class="form-group col-sm-12 col-md-5">
                <label for="phone_number_${phone.id}">Phone Number</label>
                <input type="tel"
                       id="phone_number_${phone.id}"
                       class="form-control"
                       name="phoneNumbers"
                       placeholder="Enter phone number..."
                       required
                       value="${phone.number}"
                       maxlength="50">
              </div>
              <div class="form-group col-sm-12 col-md-4">
                <label for="phone_company_${phone.id}"> Phone Company</label>
                <input type="text"
                       id="phone_company_${phone.id}"
                       class="form-control"
                       name="phoneCompanies"
                       placeholder="Enter phone company..."
                       required
                       value="${phone.phoneCompany.companyName}"
                       maxlength="50">
              </div>
              <div class="form-group col-sm-12 col-md-1 text-right">
                <a href="#" class="delete-address text-dark">
                  <svg class="bi bi-x" width="2em" height="2em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M5.646 5.646a.5.5 0 000 .708l8 8a.5.5 0 00.708-.708l-8-8a.5.5 0 00-.708 0z" clip-rule="evenodd"></path>
                    <path fill-rule="evenodd" d="M14.354 5.646a.5.5 0 010 .708l-8 8a.5.5 0 01-.708-.708l8-8a.5.5 0 01.708 0z" clip-rule="evenodd"></path>
                  </svg>
                </a>
              </div>
            </div>
            </#list>
          </#if>
        </div>
        <div class="form-row">
          <div class="form-group col-sm-12 text-right">
            <a href="#" class="btn btn-light add-address">Add Address</a>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-sm-6">
            <a class="btn btn-secondary" href="/users" role="button">Back</a>
          </div>
          <div class="form-group col-sm-6 text-right">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</#macro>

<@display_page/>
