'use strict';

$(document).ready(() => {
  if ($('.edit-class-form').length > 0) {
    $(document).on('click', '.delete-address', (event) => {
      console.log(anchor.target);
      $(event.target).parents('.form-row')
        .first()
        .remove();
    });
    $('.add-address').on('click', (event) => {
      const addressId = Math.floor(Math.random() * 100000);

      $('.edit-class-form .addresses').append(`
          <div class="form-row" data-address-id="${addressId}">
            <div class="form-group col-sm-12 col-md-2">
              <label for="phone_type_${addressId}">Phone type</label>
              <input type="text"
                     id="phone_type_${addressId}"
                     class="form-control"
                     name="phoneTypes"
                     placeholder="Enter phone type: home, work, etc..."
                     required
                     maxlength="10">
            </div>
            <div class="form-group col-sm-12 col-md-5">
              <label for="phone_number_${addressId}">Phone Number</label>
              <input type="tel"
                     id="phone_number_${addressId}"
                     class="form-control"
                     name="phoneNumbers"
                     placeholder="Enter phone number..."
                     required
                     maxlength="50">
            </div>
            <div class="form-group col-sm-12 col-md-4">
              <label for="phone_company_${addressId}"> Phone Company</label>
              <input type="text"
                     id="phone_company_${addressId}"
                     class="form-control"
                     name="phoneCompanies"
                     placeholder="Enter phone company..."
                     required
                     maxlength="50">
            </div>
            <div class="form-group col-sm-12 col-md-1 text-right">
              <a href="#" class="delete-address text-dark">
                <svg class="bi bi-x" width="2em" height="2em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" d="M5.646 5.646a.5.5 0 000 .708l8 8a.5.5 0 00.708-.708l-8-8a.5.5 0 00-.708 0z" clip-rule="evenodd"></path>
                  <path fill-rule="evenodd" d="M14.354 5.646a.5.5 0 010 .708l-8 8a.5.5 0 01-.708-.708l8-8a.5.5 0 01.708 0z" clip-rule="evenodd"></path>
                </svg>
              </a>
            </div>
          </div>`);
    });
  }
});
