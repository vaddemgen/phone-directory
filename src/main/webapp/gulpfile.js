/*global require*/
(function (r) {
  "use strict";
  const {src, dest, parallel} = require('gulp');
  const sass = require("gulp-sass");
  const minifyCSS = require('gulp-csso');
  const concat = require('gulp-concat');
  const uglify = require('gulp-uglify-es').default;

  function css() {
    return src([
      'node_modules/bootstrap/dist/css/bootstrap.css',
      'WEB-INF/scss/*.scss'
    ])
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('main.min.css'))
    .pipe(minifyCSS())
    .pipe(dest('resources/css'))
  }

  function js() {
    return src([
      'node_modules/jquery/dist/jquery.js',
      'node_modules/bootstrap/dist/js/bootstrap.js',
      'WEB-INF/js/*.js'
    ], {sourcemaps: true})
    .pipe(concat('app.min.js'))
    .pipe(uglify())
    .pipe(dest('resources/js', {sourcemaps: true}))
  }

  exports.js = js;
  exports.css = css;
  exports.default = parallel(css, js);
}(require));