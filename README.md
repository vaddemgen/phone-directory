# Phone Directory

## Simple launch with docker

- Git clone:

      git clone git@gitlab.com:vaddemgen/phone-directory.git

- Run the following command:

      docker-compose up -d --force-recreate --build

- You can fallow the web app logs:

      docker logs -f phonedirectory-app

- Go to a browser and open [http://localhost:8187](http://localhost:8187):

  - User: `vaddemgen@gmail.com`
  - Password: `q1w2e3r4`


## OR manual launch

**Project depends on JDK 11 and Tomcat 9.**

- Compile front-end dependencies.

      cd src/main/webapp
      yarn install
      ./node_modules/gulp/bin/gulp.js

- Make a WAR file:
      
      cd ../../../
      ./gradlew assemble

- Run project with JDK 11 and Tomcat 9
