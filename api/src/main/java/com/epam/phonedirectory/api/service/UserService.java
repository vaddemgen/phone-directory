package com.epam.phonedirectory.api.service;

import com.epam.phonedirectory.api.exception.ModelNotFoundException;
import com.epam.phonedirectory.api.model.Role;
import com.epam.phonedirectory.api.model.User;
import java.util.List;
import java.util.stream.Stream;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {

  Stream<User> getUsers();

  User getUser(long userId) throws ModelNotFoundException;

  void deleteUser(long userId);

  User updateUser(User user, @Nullable String password, @Nullable List<Role> roles);

  User importUser(User user);

  User createUser(User user, String password, List<Role> roles);

  Page<User> getUsers(Pageable pageable);
}
