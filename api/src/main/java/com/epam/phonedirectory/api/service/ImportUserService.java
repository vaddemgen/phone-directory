package com.epam.phonedirectory.api.service;

import com.epam.phonedirectory.api.model.User;
import java.util.Collection;
import java.util.stream.Stream;

public interface ImportUserService {

  Stream<User> importUsers(Collection<User> users);
}
