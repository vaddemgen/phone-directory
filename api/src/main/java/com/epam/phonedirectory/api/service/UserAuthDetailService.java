package com.epam.phonedirectory.api.service;

import com.epam.phonedirectory.api.model.Role;
import com.epam.phonedirectory.api.model.User;
import com.epam.phonedirectory.api.model.UserAuthDetail;
import java.util.List;
import org.jetbrains.annotations.Nullable;

public interface UserAuthDetailService {

  UserAuthDetail createUserAuthDetail(User user, String password, List<Role> roles);

  void createUserAuthDetail(User user);

  void updateUserAuthDetail(User user, @Nullable String password, @Nullable List<Role> roles);

  UserAuthDetail getUserAuthDetail(String email);

  UserAuthDetail getUserAuthDetail(long userId);

  void updateUserAuthDetail(User updatedUser, String username, @Nullable String password,
      @Nullable List<Role> roles);
}
