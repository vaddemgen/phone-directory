package com.epam.phonedirectory.api.model;

import java.util.List;
import java.util.stream.Stream;

public interface UserAuthDetail {

  Stream<Role> getRoles();

  User getUser();

  String getUsername();

  String getPassword();

  boolean isAccountNonExpired();

  boolean isAccountNonLocked();

  boolean isCredentialsNonExpired();

  boolean isEnabled();

  UserAuthDetail withPassword(String password);

  UserAuthDetail withRoles(List<Role> roles);

  UserAuthDetail withUsername(String username);
}
