package com.epam.phonedirectory.api.model;

import static java.util.Objects.nonNull;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Stream;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.jetbrains.annotations.Nullable;

@Getter
@EqualsAndHashCode(doNotUseGetters = true, exclude = "phones")
@ToString(doNotUseGetters = true)
public final class User {

  private final long id;

  @NotBlank
  @Size(max = 50)
  private final String firstName;

  @NotBlank
  @Size(max = 50)
  private final String lastName;

  @NotBlank
  @Size(max = 254)
  private final String email;

  @NotNull
  @Valid
  private final Set<PhoneNumber> phones;

  @Builder
  private User(long id, String firstName, String lastName, String email,
      @Nullable Collection<PhoneNumber> phones) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.phones = nonNull(phones) ? Set.copyOf(phones) : Set.of();
  }

  public Stream<PhoneNumber> getPhones() {
    return phones.stream();
  }

  public User withId(long id) {
    return builder()
        .id(id)
        .firstName(firstName)
        .lastName(lastName)
        .email(email)
        .phones(phones)
        .build();
  }

  public User withFirstName(String newFirstName) {
    return builder()
        .id(id)
        .firstName(newFirstName)
        .lastName(lastName)
        .email(email)
        .phones(phones)
        .build();
  }

  public User withLastName(String newLastName) {
    return builder()
        .id(id)
        .firstName(firstName)
        .lastName(newLastName)
        .email(email)
        .phones(phones)
        .build();
  }

  public User withEmail(String newEmail) {
    return builder()
        .id(id)
        .firstName(firstName)
        .lastName(lastName)
        .email(newEmail)
        .phones(phones)
        .build();
  }

  public User withPhones(Collection<PhoneNumber> newPhones) {
    return builder()
        .id(id)
        .firstName(firstName)
        .lastName(lastName)
        .email(email)
        .phones(newPhones)
        .build();
  }
}
