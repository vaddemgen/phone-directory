package com.epam.phonedirectory.api.model;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@EqualsAndHashCode(doNotUseGetters = true)
@ToString(doNotUseGetters = true)
public final class PhoneNumber {

  private final long id;

  /**
   * The type of phone, e.g. work, home
   */
  @NotBlank
  @Size(max = 50)
  private final String type;

  @NotBlank
  @Size(max = 50)
  @Pattern(regexp = "^\\+(?:[0-9] ?){6,14}[0-9]$")
  private final String number;

  @Valid
  @NotNull
  private final PhoneCompany phoneCompany;

  public PhoneNumber withType(String newType) {
    return builder()
        .id(id)
        .type(newType)
        .number(number)
        .phoneCompany(phoneCompany)
        .build();
  }

  public PhoneNumber withPhoneCompany(PhoneCompany newPhoneCompany) {
    return builder()
        .id(id)
        .type(type)
        .number(number)
        .phoneCompany(newPhoneCompany)
        .build();
  }
}
