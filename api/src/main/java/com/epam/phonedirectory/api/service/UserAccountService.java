package com.epam.phonedirectory.api.service;

import com.epam.phonedirectory.api.exception.NotEnoughMoneyException;
import com.epam.phonedirectory.api.exception.UserAccountNotFoundException;
import com.epam.phonedirectory.api.model.PhoneNumber;
import com.epam.phonedirectory.api.model.User;
import com.epam.phonedirectory.api.model.UserAccount;
import java.util.stream.Stream;

public interface UserAccountService {

  void createDefaultUserAccount(User user, PhoneNumber number);

  Stream<UserAccount> getAccounts(User user);

  void deleteUserAccount(PhoneNumber phoneNumber);

  void topUpAccount(User user, long accountId, double money) throws UserAccountNotFoundException;

  void changeOperator(User user, long accountId, long newOperatorId)
      throws NotEnoughMoneyException, UserAccountNotFoundException;
}
