package com.epam.phonedirectory.api.service;

import com.epam.phonedirectory.api.model.PhoneNumber;
import com.epam.phonedirectory.api.model.User;

public interface PhoneNumberService {

  PhoneNumber importPhoneNumber(PhoneNumber model, User owner);
}
