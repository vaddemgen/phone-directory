package com.epam.phonedirectory.api.model;

import java.util.Comparator;

public final class PhoneCompanyComparator implements Comparator<PhoneCompany> {

  @Override
  public int compare(PhoneCompany o1, PhoneCompany o2) {
    return Comparator.comparing(PhoneCompany::getCompanyName)
        .compare(o1, o2);
  }
}
