package com.epam.phonedirectory.api.exception;

import com.epam.phonedirectory.api.model.UserAccount;
import java.text.MessageFormat;
import java.text.NumberFormat;

public class NotEnoughMoneyException extends Exception {

  private static final long serialVersionUID = 5634516956537082883L;

  public NotEnoughMoneyException(UserAccount userAccount, double requiredAmount) {
    super(MessageFormat.format(
        "You don''t have enough money for this operation: required {0}, you have only {1}",
        NumberFormat.getCurrencyInstance().format(requiredAmount),
        NumberFormat.getCurrencyInstance().format(userAccount.getAmount())
    ));
  }
}
