package com.epam.phonedirectory.api.exception;

import java.text.MessageFormat;

public final class UserAccountNotFoundException extends Exception {

  private static final long serialVersionUID = 569615977037149070L;

  public UserAccountNotFoundException(long accountId) {
    super(MessageFormat.format("User Account ''{0}'' not found.", accountId));
  }
}
