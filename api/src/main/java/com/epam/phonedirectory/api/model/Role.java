package com.epam.phonedirectory.api.model;

import java.text.MessageFormat;
import java.util.stream.Stream;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum Role {

  USER("ROLE_REGISTERED_USER"),
  BOOKING_MANAGER("ROLE_BOOKING_MANAGER");

  private final String value;

  public static Role fromString(String strValue) {
    return Stream.of(values())
        .filter(role -> role.value.endsWith(strValue))
        .findAny()
        .orElseThrow(() -> new IllegalArgumentException(
            MessageFormat.format("A constant wasn't found for value ''{0}''.", strValue)));
  }
}
