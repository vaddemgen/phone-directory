package com.epam.phonedirectory.api.service;

import com.epam.phonedirectory.api.model.PhoneCompany;
import java.util.stream.Stream;

public interface PhoneCompanyService {

  PhoneCompany getPhoneCompany(long phoneCompanyId);

  PhoneCompany importPhoneCompany(PhoneCompany model);

  Stream<PhoneCompany> getAll();
}
