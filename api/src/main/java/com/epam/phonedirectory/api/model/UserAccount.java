package com.epam.phonedirectory.api.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@EqualsAndHashCode(doNotUseGetters = true, of = "id")
@ToString(doNotUseGetters = true)
public final class UserAccount {

  private final long id;

  @NotNull
  private final User user;

  @Min(0)
  private final double amount;

  @NotNull
  private final PhoneNumber phoneNumber;

  @NotNull
  private final PhoneCompany operator;

  public UserAccount withOperator(@NonNull PhoneCompany newOperator) {
    return builder()
        .id(id)
        .user(user)
        .amount(amount)
        .phoneNumber(phoneNumber)
        .operator(newOperator)
        .build();
  }

  public UserAccount withAmount(double newAmount) {
    return builder()
        .id(id)
        .amount(newAmount)
        .user(user)
        .phoneNumber(phoneNumber)
        .operator(operator)
        .build();
  }
}
