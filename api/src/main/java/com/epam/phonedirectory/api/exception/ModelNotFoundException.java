package com.epam.phonedirectory.api.exception;

public class ModelNotFoundException extends Exception {

  private static final long serialVersionUID = 6559982508414677522L;

  public ModelNotFoundException(String message) {
    super(message);
  }
}
